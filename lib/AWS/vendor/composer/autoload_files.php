<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => $vendorDir . '/symfony/polyfill-mbstring/bootstrap.php',
    'c964ee0ededf28c96ebd9db5099ef910' => $vendorDir . '/guzzlehttp/promises/src/functions_include.php',
    'a0edc8309cc5e1d60e3047b5df6b7052' => $vendorDir . '/guzzlehttp/psr7/src/functions_include.php',
    '37a3dc5111fe8f707ab4c132ef1dbc62' => $vendorDir . '/guzzlehttp/guzzle/src/functions_include.php',
    '5255c38a0faeba867671b61dfda6d864' => $vendorDir . '/paragonie/random_compat/lib/random.php',
    '72579e7bd17821bb1321b87411366eae' => $vendorDir . '/illuminate/support/helpers.php',
    '04c6c5c2f7095ccf6c481d3e53e1776f' => $vendorDir . '/mustangostang/spyc/Spyc.php',
    '19cefe1485315b72c45605e5be32d866' => $vendorDir . '/donatj/phpuseragentparser/Source/UserAgentParser.php',
    '23a734c96aaa09a62ea05a63edf5c9e3' => $vendorDir . '/zsxsoft/php-useragent/useragent.class.php',
    '7809bc348feba2e050a7bb7b01a7dd4b' => $vendorDir . '/zsxsoft/php-useragent/lib/useragent_detect_browser.php',
    '7e6b8ebd95372cd70bb996e274ce3557' => $vendorDir . '/zsxsoft/php-useragent/lib/useragent_detect_device.php',
    '3046c8dd955609a20c0a979ca7f4cc09' => $vendorDir . '/zsxsoft/php-useragent/lib/useragent_detect_os.php',
);
