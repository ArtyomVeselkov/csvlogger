### 3.0.0
+ added bots detection (+):
    - Search Engines:
        + Google
        + Yandex
        + Bing
        + Yodao
    - RSS Readers:
        + Tyny RSS
    - Bot/Crawlers:
        + WASALive
        + .NET Framework CLR
        + 007ac9 Crawler
        + 80legs Crawler
        + 123metaspider Crawler
        + 1470 Crawler
        + Yisou Spider
        + Yioop Bot
+ added os detection (+):
    - AIX
    - Aliyun OS
    - Amiga
    - Android (TV)
    - Apple (TV)
+ added device detection (+)
    - Apple TV
+ fixed issues:
    - gh-2
    - gh-3

### v.2.0
+ code refactoring
