<?php
/**
 * @author Sergey Nehaenko <sergey.nekhaenko@gmail.com>
 * @license GPL
 * @copyright Sergey Nehaenko &copy 2016
 * @version 3.0
 * @project browser-detector
 */

use EndorphinStudio\Detector\Detector;

$result = Detector::analyse();
