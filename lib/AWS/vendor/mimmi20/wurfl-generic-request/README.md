Wurfl Generic Request
=====================

extracted the GenericRequest object from Wurfl

[![Build Status](https://api.travis-ci.org/mimmi20/wurfl-generic-request.png?branch=master)](https://travis-ci.org/mimmi20/wurfl-generic-request)
[![Scrutinizer Quality Score](https://scrutinizer-ci.com/g/mimmi20/wurfl-generic-request/badges/quality-score.png?s=5e88e19d3a659f74ca468170d70c30c94c4ab2c0)](https://scrutinizer-ci.com/g/mimmi20/wurfl-generic-request/)
[![Code Coverage](https://scrutinizer-ci.com/g/mimmi20/wurfl-generic-request/badges/coverage.png?s=b9a661d611e63c513c3d6800572c3f06e520bae4)](https://scrutinizer-ci.com/g/mimmi20/wurfl-generic-request/)

Submitting bugs and feature requests
------------------------------------

Bugs and feature request are tracked on [GitHub](https://github.com/mimmi20/wurfl-generic-request/issues)
