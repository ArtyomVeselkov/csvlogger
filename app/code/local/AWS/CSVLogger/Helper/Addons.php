<?php

class AWS_CSVLogger_Helper_Addons extends Mage_Core_Helper_Data
{
    const DEFAULT_ADDON_CALL_COOKIES_LIFETIME = 60;

    protected $_defaultAddonCallCookiesLifetime = 60;
    protected $_defaultAddonCallModelLifeTime = 60;
    protected $_defaultAddonCallModelCallTimes = 1;

    protected $_featureAddons = null;
    protected $_handlesAction = null;
    protected $_enabledAddons = null;

    public function process($eventName, $eventData, &$registers)
    {
        $status = array();
        if (is_null($this->_featureAddons)) {
            $this->_featureAddons = $this->loadAddons();
        }
        if (is_null($this->_handlesAction)) {
            $this->_prepareHandlesAction();
        }
        if (isset($this->_handlesAction[$eventName])) {
            $addonRepo = Mage::helper('aws_csvlogger/addonsRepo');
            foreach ($this->_handlesAction[$eventName] as $method) {
                $status = array();
                if (method_exists($addonRepo, $method)) {
                    if (!isset($addonRepo::$_buffer[$method])) {
                        $addonRepo::$_buffer[$method] = array();
                    }
                    $addonRepo::$_eventName = $eventName;
                    $status = $addonRepo->{$method}($registers, $eventData);
                    $addonRepo::$_eventName = '';
                    if (is_array($status) && isset($status['error'])) {
                        return $status;
                    }
                }
            }
        }
        return $status;
    }

    public function addRuntimeAddon($method, $data)
    {
        if (strlen($method) && is_array($data) && !isset($this->_featureAddons[$method])) {
            $this->_featureAddons[$method] = $data;
            $this->_prepareHandlesAction();
            return true;
        }
        return false;
    }

    protected function loadAddons($allAddons = false)
    {
        $allAddonds = Mage::helper('aws_csvlogger/addonsRepo')->getRegisteredAddons();
        $registeredAddons = array();
        $enabledAddonsConfig = Mage::getStoreConfig('aws_csvlogger/addons_options/enabled_addons');
        if ($allAddons) {
            $registeredAddons = $allAddonds;
        } elseif (is_string($enabledAddonsConfig) || strlen($enabledAddonsConfig)) {
            $enabledAddonsIds = @explode(',', $enabledAddonsConfig);
            foreach ($enabledAddonsIds as $enabledAddonsId) {
                if (isset($allAddonds[$enabledAddonsId])) {
                    try {
                        $registeredAddons[$enabledAddonsId] = $allAddonds[$enabledAddonsId];
                    } catch (\Exception $exception) {
                        ;
                    }
                }
            }
        }
        return $registeredAddons;
    }

    public function getAllAddons()
    {
        if (is_null($this->_featureAddons)) {
            $this->_featureAddons = $this->loadAddons(true);
        }
        foreach ($this->_featureAddons as $addonName => $addonData) {
            $options[] = array(
                'value' => $addonName,
                'label' => isset($addonData['label']) && 3 < strlen($addonData['label']) ? $addonData['label'] : $addonName
            );
        }
        return $options;
    }

    public function isAddonEnabled($addonName)
    {
        if (is_null($this->_enabledAddons)) {
            $this->_enabledAddons = explode(',', Mage::getStoreConfig('aws_csvlogger/addons_options/enabled_addons'));
        }
        return false !== array_search($addonName, $this->_enabledAddons);
    }

    protected function _prepareHandlesAction()
    {
        foreach ($this->_featureAddons as $addonName => $addonData) {
            if (isset($addonData['fire_on'])) {
                foreach ($addonData['fire_on'] as $addonEvent) {
                    if (!isset($this->_handlesAction[$addonEvent])) {
                        $this->_handlesAction[$addonEvent] = array();
                    }
                    $this->_handlesAction[$addonEvent][] = $addonName;
                }
            }
        }
    }

    protected function _restoreAddonEncKey()
    {
        $newKey = Mage::helper('core')->getRandomString(32);
        Mage::getConfig()->saveConfig(
            AWS_CSVLogger_Helper_Data::MAGE_CORE_CONFIG_SALT_FOR_HASH_INTER_CTRL_KEY,
            $newKey
        );
        return $newKey;
    }

    public function newHash()
    {
        return md5(uniqid(mt_rand(), true));
    }
    
    protected function _prepareSecurityData($params = array())
    {
        $data = array();
        $data['params'] = is_array($params) && 0 < count($params) ? $params : Mage::app()->getRequest()->getParams();
        
        // 1. prepare key
        // TODO perhaps need to direct 
        $data['addon_key'] = Mage::helper('aws_csvlogger')->getStoreConfigValueDirect(AWS_CSVLogger_Helper_Data::MAGE_CORE_CONFIG_SALT_FOR_HASH_INTER_CTRL_KEY);
        if (is_null($data['addon_key']) || 3 > strlen($data['addon_key'])) {
            $data['addon_key'] = $this->_restoreAddonEncKey();
        }

        // 2. load encryptor
        $data['mage_encryptor'] = Mage::getModel('core/encryption');
        $data['varien_encryptor'] = Mage::getModel('core/encryption')->validateKey($data['addon_key']);

        // 3. get current session
        $data['session'] = Mage::app()->getStore()->isAdmin() ? Mage::getSingleton('admin/session') : Mage::getSingleton('core/session');

        // 4. remove admins parameters
        try {
            unset($data['params']['key']);
            unset($data['params']['isAjax']);
            unset($data['params']['_cache_secret_key']);
            unset($data['params']['_store']);
            unset($data['params']['_type']);
            unset($data['params']['_nosecret']);
            // and some others
        } catch (Exception $e) {
            ;
        }

        // 5. prepare user
        $data['user'] = null;
        if (is_a($data['session'], 'Mage_Admin_Model_Session')) {
            $data['user'] = $data['session']->getUser();
        } else {
            $data['user'] = $data['session']->getCustomer();
        }
        
        // 6. prepare addon call
        $data['addon_call'] = Mage::getModel('aws_csvlogger/addonscalls');
        
        // 7. prepare cookies
        $data['cookies'] = $data['session']->getCookie();
        
        return $data;
    }

    public function holdSecurityData($params = array())
    {
        $data = $this->_prepareSecurityData($params);
        
        $params = @$data['params'];
        $encryptorMage = @$data['mage_encryptor'];
        $encryptorVarien = @$data['varien_encryptor'];
        $session = @$data['session'];
        $user = @$data['user'];
        $cookies = @$data['cookies'];
        $addonCall = @$data['addon_call'];
        
        $result = array();

        // 1. encode parameters
        $encParams = '';
        $encParamsHash1 = '';
        $encParamsHash2 = '';
        $hash = $this->newHash();
        $cryptedHash = base64_encode($encryptorVarien->encrypt($encryptorMage->encrypt($hash)));
        try {
            $encParams = $encryptorVarien->encrypt(json_encode($params));
            $encParamsHash1 = base64_encode($encryptorVarien->encrypt(md5($encParams)));
            $encParamsHash2 = base64_encode($encryptorMage->encrypt(md5($encParams)));
        } catch (Exception $e) {
            ;
        }
        
        // 2. save model
        $addonCall->setHash(trim($hash))
            ->setEsiMd5(!is_null($session->getEncryptedSessionId()) ? md5($session->getEncryptedSessionId()) : null)
            ->setUserCaller(
                sprintf('%s %s (%s)', $user->getFirstname(), $user->getLastname(), $user->getEmail())
            )
            ->setParams($encParams)
            ->setEnabled(true)
            ->save();
        
        // 3. set cookies
        $cookies->set('aws_csvlogger_params', $encParamsHash1, $this->_defaultAddonCallCookiesLifetime);
        
        // 4. return GET parameters
        $result['hash1'] = rawurlencode($cryptedHash);
        $result['hash2'] = rawurlencode($encParamsHash2);

        return http_build_query($result);
    }
    
    public function unholdSecurityData($setAsUsedCall = true)
    {
        $result = false;
        $data = $this->_prepareSecurityData();

        $params = @$data['params'];
        $encryptorMage = @$data['mage_encryptor'];
        $encryptorVarien = @$data['varien_encryptor'];
        $session = @$data['session'];
        $user = @$data['user'];
        $cookies = @$data['cookies'];
        $addonCall = @$data['addon_call'];

        $flags = array();

        // 1. get hash from GET and cookie
        $cryptedHash = rawurldecode(@$params['hash1']);
        $encParamsHash2 = base64_decode(rawurldecode(@$params['hash2']));

        // 2. get hash from cookies
        $encParamsHash1 = base64_decode($cookies->get('aws_csvlogger_params'));

        // 3. try decode hash
        $hash = $encryptorMage->decrypt($encryptorVarien->decrypt(base64_decode($cryptedHash)));

        // 4. load model
        $addonCalls = Mage::getResourceSingleton('aws_csvlogger/addonscalls_collection')->getActiveAddonsCallByHash($hash)->load();
        if (0 < count($addonCalls)) {
            try {
                $addonCall = $addonCalls->getLastItem();
            } catch (Exception $e) {
                Mage::log('CSVLogger > Addon Frontend Controller - Error while retrieving Addon Call (' . $e->getMessage() . ')', null, 'csvlogger.log');
            }
        } else {
            $addonCall = null;
            return false;
        }

        // 5. get params and corresponded hashes
        $cleanParams = (array)json_decode(trim(utf8_encode($encryptorVarien->decrypt($addonCall->getParams()))), true);
        $encParamsHash1DB = $encryptorVarien->encrypt(md5($addonCall->getParams()));
        $encParamsHash2DB = $encryptorMage->encrypt(md5($addonCall->getParams()));

        // 6. collect conditions
        if (is_null($addonCall)) {
            return false;
        }
        $flags[] = (bool)$addonCall->getEnabled();
        $flags[] = $encParamsHash1DB === $encParamsHash1;
        $flags[] = $encParamsHash2DB === $encParamsHash2;
        $flags[] = time() - strtotime($addonCall->getCreationTime()) < $this->_defaultAddonCallModelLifeTime;
        $flags[] = $addonCall->getCallTimes() < $this->_defaultAddonCallModelCallTimes;

        $flag = 1 === count(array_unique($flags)) ? $flags[0] : false;

        // 7. clean data anyway
        if ($setAsUsedCall) {
            $cookies->delete('aws_csvlogger_params');
            if (!is_null($addonCall)) {
                // to be "pedantic"
                $addonCall->setEnabled(false);
                $addonCall->setCallTimes($addonCall->getCallTimes() + 1);
                $addonCall->isDeleted(true);
                $addonCall->save();
            }
        }

        return $flag ? $cleanParams : $flag;
    }

    public function __prepareSecurityData($addonName, $addonParams, &$data = array())
    {
        $data = array();
        $session = Mage::app()->getStore()->isAdmin() ? Mage::getSingleton('admin/session') : Mage::getSingleton('core/session');
        $cookies = $session->getCookie();
        $data['esi_hashed'] = md5($session->getEncryptedSessionId());
        $data['hased_data'] = $addonName . md5(json_encode($addonParams));;
        $cookies->set('aws_csvlogger_' . $addonName, $data['hased_data'], 20);
        $cookies->set('aws_csvlogger_esi', $data['esi_hashed'], 20);
    }

    // TODO 2 x functions: holdSecurityData and check (which removes cookies)
    // TODO new config_data -- install script with random
    public function _checkSecurityData($addonName, $addonParams, $inputData, $currentData)
    {
        $session = Mage::app()->getStore()->isAdmin() ? Mage::getSingleton('admin/session') : Mage::getSingleton('core/session');
        $cookies = $session->getCookie();

        try {
            $flagEsiData = $inputData['esi_hashed'] === $currentData['esi_hashed'];
            $flagHashedData = $inputData['hased_data'] === $currentData['hased_data'];
            $flagEsiCookies = $cookies->get('aws_csvlogger_esi') === $currentData['esi_hashed'];
            $flagHashedCookies = $cookies->get('aws_csvlogger_' . $addonName) === $currentData['hased_data'];
        } catch (Exception $e) {
                return false;
        }

        try {
            $cookies->delete('aws_csvlogger_esi');
            $cookies->delete('aws_csvlogger_' . $addonName);
        } catch (Exception $e) {
            ;
        }
    }

    public static function checkHandle(array $handles)
    {
        if (0 == count($handles)) {
            return true;
        }
        $actualHandles = @Mage::app()->getLayout()->getUpdate()->getHandles();
        $intersect = array_intersect($actualHandles, $handles);
        return 0 < count($intersect);
    }

    public static function checkBacktrace(array $backtraceAccept, array $backtraceExit)
    {
        if (empty($backtraceAccept) && empty($backtraceExit)) {
            return true;
        }
        $result = false;
        $backtrace = array();
        try {
            // yeh, we get it
            $backtrace = @debug_backtrace();
            foreach ($backtrace as $step => $stepData) {
                $stepFunction = @$stepData['function'];
                if (in_array($stepFunction, $backtraceExit)) {
                    $result = false;
                    return $result;
                } elseif (in_array($stepFunction, $backtraceAccept)) {
                    $result = true;
                    return $result;
                }
            }
        } catch (Exception $exception) {
            return $result;
        }
        return $result;
    }

    public static function checkController($controllers)
    {
        if (false === $controllers) {
            return true;
        }
        if (!is_array($controllers)) {
            $controllers = array($controllers);
        }
        $fullActionName = static::getFullActionName();
        if (!is_string($fullActionName)) {
            return false;
        }
        $matchFlag = false;
        foreach ($controllers as $controllerMask) {
            $bufferFlag = false;
            if (strlen($controllerMask)) {
                if ('/' == ($controllerMask[0])) {
                    $bufferFlag = preg_match($controllerMask, $fullActionName);
                } else {
                    $bufferFlag = $controllerMask == $fullActionName;
                }
            }
            if ($bufferFlag) {
                $matchFlag = true;
                break;
            }
        }
        return $matchFlag;
    }

    public static function getFullActionName()
    {
        $actualController = @Mage::app()->getFrontController();
        if (!$actualController || !is_object(@$actualController->getAction())) {
            return false;
        }
        try {
            // over-check to make work API call from Signifyd due to reassigned request to array type and magento exception crash
            if (is_a(@$actualController->getAction()->getRequest(), 'Mage_Core_Controller_Request_Http')) {
                $fullActionName = @$actualController->getAction()->getFullActionName();
            } else {
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }
        return $fullActionName;
    }
}