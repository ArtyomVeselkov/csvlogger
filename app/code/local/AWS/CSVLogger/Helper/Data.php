<?php

class AWS_CSVLogger_Helper_Data extends Mage_Core_Helper_Data
{
    const MAGE_CORE_CONFIG_SALT_FOR_HASH_INTER_CTRL_KEY = 'aws_csvlogger/inter_ctrl/key';
    const DEFAULT_LOG_FILE = 'csvlogger.log';

    // this is list of accessible parameters for safety purpose
    // total 54+ field
    const VAR_ID = 'var_id';
    const VAR_SESSION_ID = 'session_id';
    const VAR_PRODUCT_ID_ADDED = 'product_added_id';
    const VAR_REMOTE_IP = 'ip';
    const VAR_CUSTOMER_ID = 'customer_id';
    const VAR_VISITOR_ID = 'visitor_id';
    const VAR_CUSTOMER_EMAIL = 'customer_email';
    const VAR_QUOTE_ID = 'quote_id';
    const VAR_ORDER_ID = 'order_id';
    const VAR_ORDER_RESERVED_ID = 'order_reserved_id';
    const VAR_QUOTE_ORIGIN_ORDER_ID = 'quote_orig_order_id';
    const VAR_INVOICE_ID = 'invoice_id';
    const VAR_SHIPMENT_METHOD = 'shipment_method';
    const VAR_SHIPMENT_TOTAL = 'shipment_total';
    const VAR_ORDER_APPLIED_RULE_IDS = 'applied_rule_ids';
    const VAR_ORDER_TOTAL_QTY = 'total_qty';
    const VAR_ORDER_TOTAL_ITEMS = 'total_item_count';
    const VAR_SUBTOTAL = 'subtotal';
    const VAR_GRANDTOTAL = 'grand_total';
    const VAR_SHIPPING_METHOD = 'shipping_method';
    const VAR_SHIPPING_DESCRIPTION = 'shipping_description';
    const VAR_SHIPPING_RATE = 'shipping_rate';
    const VAR_TAX_AMOUNT = 'tax_amount';
    const VAR_TAX_STRING = 'tax_string';
    const VAR_GRAND_TOTAL = 'grand_total';
    const VAR_BILLING_ADDRESS_ID = 'billing_address_id';
    const VAR_SHIPPING_ADDRESS_ID = 'shipping_address_id';
    const VAR_TAX_APPLIED_SAVED = 'applied_tax_is_saved';
    const VAR_ORDER_PROTEC_CODE = 'order_protect_code';
    const VAR_ORDER_STATE = 'order_state';
    const VAR_ORDER_STATUS = 'order_status';
    const VAR_ORDER_TO_QUOTE = 'converting_from_quote';
    const VAR_QUOTE_CREATED_AT = 'quote_created_at';
    const VAR_QUOTE_CONVERTED_AT = 'quote_converted_at';
    const VAR_CHECKOUT_METHOD = 'checkout_method';
    const VAR_CUSTOMER_TAX_ID = 'customer_tax_class_id';
    const VAR_QUOTE_ACTIVE = 'quote_active';
    const VAR_PAYMENT_ID = 'payment_id';
    const VAR_PAYMENT_METHOD = 'payment_method';
    const VAR_PAYMENT_CREATED_AT = 'payment_created_at';
    const VAR_PAYMENT_UPDATED_AT = 'payment_updated_at';
    const VAR_PAYMENT_CC_TYPE = 'payment_cc_type';
    const VAR_PAYMENT_CC_LAST4 = 'payment_cc_last4';
    const VAR_PAYMENT_CC_OWNER = 'payment_cc_owner';
    const VAR_PAYMENT_SS_ISSUE = 'payment_cc_ss_issue';
    const VAR_PAYMENT_PAYPAL_CORRELATION_ID = 'paypal_correlation_id';
    const VAR_PAYMENT_PAYPAL_PAPER = 'payment_paypal_paper_id';
    const VAR_PAYMENT_CHECKS = 'payment_checks';
    const VAR_PAYMENT_CC_NUMBER = 'payment_cc_number';
    const VAR_PAYMENT_CC_CID = 'payment_cc_cid';
    const VAR_PARENT_ID = 'payment_parent_id';
    const VAR_PAYMENT_AMOUNT_ORDERED = 'payment_amount_ordered';
    const VAR_PAYMENT_AMOUNT_SHIPPING = 'payment_shipping_amount';
    
    const VAR_CUSTOMER_FIRSTNAME = 'customer_firstname';
    const VAR_CUSTOMER_LASTNAME = 'customer_lastname';
    const VAR_CUSTOMER_DEFAULT_BILLING = 'customer_default_billing_id';
    const VAR_CUSTOMER_DEFAULT_SHIPPING = 'customer_default_shipping_id';
    const VAR_CUSTOMER_GROUP_ID = 'customer_group_id';
    const VAR_CUSTOMER_CREATED_AT = 'created_at';
    const VAR_CUSTOMER_IS_GUEST = 'is_guest';

    const VAR_PAYMENT_ERROR_ORDER_INC_ID = 'error_payment_order_inc_id';

    const VAR_CUSTOMER_ADDRESS_ID = 'customer_address_id';
    const VAR_CUSTOMER_ADDRESS_CITY = 'customer_address_city';
    const VAR_CUSTOMER_ADDRESS_POSTCODE = 'customer_address_postcode';
    const VAR_CUSTOMER_ADDRESS_STREET = 'customer_address_street';
    const VAR_CUSTOMER_ADDRESS_TELEPHONE = 'customer_address_telephone';
    const VAR_CUSTOMER_ADDRESS_COUNTRY_ID = 'customer_address_country_id';
    const VAR_CUSTOMER_ADDRESS_TYPE = 'customer_address_type';

    const VAR_SALES_COUPON_CODE_ORIGIN = 'coupon_code_origin';
    const VAR_SALES_COUPON_CODE_FIXED = 'coupon_code_fixed';
    const VAR_SALES_COUPON_CODE_INFO = 'coupon_code_info';
    const VAR_SALES_COUPON_CODE_QUOTE = 'coupon_code_quote';
    
    const VAR_POST_PARAMS = 'params';

    const VAR_PAYMENT_PLACE_ORDER_ERROR = 'payment_place_order_error';
    const VAR_DETECT_UA_RESULT = 'detect_ua_result';

    const VAR_PAYMENT_FRAUD_AVS = 'payment_avs_result';
    const VAR_PAYMENT_FRAUD_CSC = 'payment_csc_result';
    const VAR_PAYMENT_FRAUD_FRAUD = 'payment_fraud_result';
    const VAR_PAYMENT_TXN_RESULT = 'payment_txn_result';
    const VAR_PAYMENT_FRAUD_METHOD = 'payment_fraud_method';

    const VAR_ADDITIONAL_INFO = 'additional_info';

    protected $vc = array(
        self::VAR_ID,
        self::VAR_SESSION_ID,
        self::VAR_PRODUCT_ID_ADDED,
        self::VAR_REMOTE_IP,
        self::VAR_CUSTOMER_ID,
        self::VAR_VISITOR_ID,
        self::VAR_CUSTOMER_EMAIL,
        self::VAR_QUOTE_ID,
        self::VAR_ORDER_ID,
        self::VAR_ORDER_RESERVED_ID,
        self::VAR_QUOTE_ORIGIN_ORDER_ID,
        self::VAR_INVOICE_ID,
        self::VAR_SHIPMENT_METHOD,
        self::VAR_SHIPMENT_TOTAL,
        self::VAR_ORDER_APPLIED_RULE_IDS,
        self::VAR_ORDER_TOTAL_QTY,
        self::VAR_ORDER_TOTAL_ITEMS,
        self::VAR_SUBTOTAL,
        self::VAR_GRANDTOTAL,
        self::VAR_SHIPPING_METHOD,
        self::VAR_SHIPPING_DESCRIPTION,
        self::VAR_SHIPPING_RATE,
        self::VAR_TAX_AMOUNT,
        self::VAR_TAX_STRING,
        self::VAR_GRAND_TOTAL,
        self::VAR_BILLING_ADDRESS_ID,
        self::VAR_SHIPPING_ADDRESS_ID,
        self::VAR_TAX_APPLIED_SAVED,
        self::VAR_ORDER_PROTEC_CODE,
        self::VAR_ORDER_STATE,
        self::VAR_ORDER_STATUS,
        self::VAR_ORDER_TO_QUOTE,
        self::VAR_QUOTE_CREATED_AT,
        self::VAR_QUOTE_CONVERTED_AT,
        self::VAR_CHECKOUT_METHOD,
        self::VAR_CUSTOMER_TAX_ID,
        self::VAR_QUOTE_ACTIVE,
        self::VAR_PAYMENT_ID,
        self::VAR_PAYMENT_METHOD,
        self::VAR_PAYMENT_CREATED_AT,
        self::VAR_PAYMENT_UPDATED_AT,
        self::VAR_PAYMENT_CC_TYPE,
        self::VAR_PAYMENT_CC_LAST4,
        self::VAR_PAYMENT_CC_OWNER,
        self::VAR_PAYMENT_SS_ISSUE,
        self::VAR_PAYMENT_PAYPAL_CORRELATION_ID,
        self::VAR_PAYMENT_PAYPAL_PAPER,
        self::VAR_PAYMENT_CHECKS,
        self::VAR_PAYMENT_CC_NUMBER,
        self::VAR_PAYMENT_CC_CID,
        self::VAR_PARENT_ID,
        self::VAR_PAYMENT_AMOUNT_ORDERED,
        self::VAR_PAYMENT_AMOUNT_SHIPPING,

        self::VAR_CUSTOMER_FIRSTNAME,
        self::VAR_CUSTOMER_LASTNAME,
        self::VAR_CUSTOMER_DEFAULT_BILLING,
        self::VAR_CUSTOMER_DEFAULT_SHIPPING,
        self::VAR_CUSTOMER_GROUP_ID,
        self::VAR_CUSTOMER_CREATED_AT,

        self::VAR_CUSTOMER_IS_GUEST,
        self::VAR_PAYMENT_ERROR_ORDER_INC_ID,

        self::VAR_CUSTOMER_ADDRESS_ID,
        self::VAR_CUSTOMER_ADDRESS_CITY,
        self::VAR_CUSTOMER_ADDRESS_POSTCODE,
        self::VAR_CUSTOMER_ADDRESS_STREET,
        self::VAR_CUSTOMER_ADDRESS_TELEPHONE,
        self::VAR_CUSTOMER_ADDRESS_COUNTRY_ID,
        self::VAR_CUSTOMER_ADDRESS_TYPE,

        self::VAR_SALES_COUPON_CODE_ORIGIN,
        self::VAR_SALES_COUPON_CODE_FIXED,
        self::VAR_SALES_COUPON_CODE_INFO,
        
        self::VAR_POST_PARAMS,
        self::VAR_PAYMENT_PLACE_ORDER_ERROR,

        self::VAR_SALES_COUPON_CODE_QUOTE,
        self::VAR_DETECT_UA_RESULT,

        self::VAR_PAYMENT_FRAUD_AVS,
        self::VAR_PAYMENT_FRAUD_CSC,
        self::VAR_PAYMENT_FRAUD_FRAUD,
        self::VAR_PAYMENT_TXN_RESULT,
        self::VAR_PAYMENT_FRAUD_METHOD,

        self::VAR_ADDITIONAL_INFO
    );

    protected $_excludedUrls = null;

    protected $translated = array(
        'Customer ID' => 'Customer ID',
        'Guest ID' => 'Guest ID',
        'Order ID' => 'Order ID',
        'Reserved ID' => 'Reserved ID',
        'Method' => 'Method'
    );

    protected static $_dict = array();
    protected static $_dict_loaded = false;

    protected static function _fmtColumn()
    {
        $index = 0;
        $blank = false;
        $columnData = func_get_args();
        return implode(array_map(
            function ($col) use (&$index, &$blank) {
                $index++;
                if (!$blank) {
                    $blank = ($index % 2 != 0) && (null == $col);
                } else {
                    $blank = false;
                    return '';
                }
                return (!$blank) && (!empty($col)) ? '{{' . $col . '}} ' : '';
            },
            $columnData
        ));
    }

    public static function replaceStringByMask($string, $array)
    {
        // Anonymous functions become available after PHP 5.3
        return preg_replace_callback('/{{(.*?)}}/', function ($fields) use ($array) {
            return isset($array[$fields[1]]) ? $array[$fields[1]] : 'null';
        }, $string);
    }

    protected function generateLogName()
    {
        return $this::replaceStringByMask(
            Mage::getStoreConfig('aws_csvlogger/options/log_file_mask'),
            array('date' => date("Y-m-d"))
        );
    }

    public function getColumnsLayout_old($columnsKeys)
    {
        $ca = array_combine($columnsKeys, $columnsKeys);
        $__ic = function ($key) use ($ca) {
            return isset($ca[$key]) ? $key : null;
        };
        return array(
            '#' => static::_fmtColumn($__ic(self::VAR_ID), ''),
            'session #' => static::_fmtColumn($__ic(self::VAR_SESSION_ID), ''),
            'user #' => static::_fmtColumn($__ic(self::VAR_CUSTOMER_ID), 'Customer ID',
                $__ic(self::VAR_VISITOR_ID), 'Guest ID'),
            'email' => static::_fmtColumn($__ic(self::VAR_CUSTOMER_EMAIL), ''),
            'ip' => static::_fmtColumn($__ic(self::VAR_REMOTE_IP), ''),
            'quote #' => static::_fmtColumn($__ic(self::VAR_QUOTE_ID), ''),
            'order #' => static::_fmtColumn($__ic(self::VAR_ORDER_ID), 'Order ID',
                $__ic(self::VAR_ORDER_RESERVED_ID), 'Reserved ID'),
            'invoice #' => static::_fmtColumn($__ic(self::VAR_INVOICE_ID), ''),
            'payment' => static::_fmtColumn($__ic(self::VAR_PAYMENT_METHOD), 'Method'),
            'shipment' => static::_fmtColumn($__ic(self::VAR_SHIPMENT_METHOD), 'Method')
        );
    }

    public static function parseKeyValuePair($source)
    {
        $items = array();
        $result = array();
        if (is_string($source)) {
            $items = preg_split('/$\R?^/m', $source);
        } elseif (is_array($source)) {
            $items = $source;
        } else {
            $items = array($source);
        }

        foreach ($items as &$item) {
            $item = trim($item);
            switch (@$item[0])
            {
                case '#':
                    continue;
                    break;
                default:
                    $itemParts = explode('=', $item);
                    if (count($itemParts) > 1) {
                        $result[trim($itemParts[0])] = trim($itemParts[1]);
                    }
                    break;
            }
        }
        return $result;
    }

    public function getColumnsLayout()
    {
        return static::parseKeyValuePair(Mage::getStoreConfig('aws_csvlogger/options/log_columns_layout'));
    }
    
    public function getExcludedUrls($lines, $forceRebuild = false)
    {
        // disable caching logic
        if (false && !is_null($this->_excludedUrls) && !$forceRebuild) {
            return $this->_excludedUrls;
        }
        $exclude = array();
        $items = preg_split('/$\R?^/m', $lines);
        foreach ($items as &$item) {
            $item = trim($item);
            if (!isset($item[0])) {
                continue;
            }
            switch ($item[0])
            {
                case '#':
                    continue;
                    break;
                default:
                    if ('' != $item) {
                        // treat samples as follows: ^\/admin\/.* | adminhtml_widget_container_html_before, adminhtml_widget_index
                        $parts = explode('|', $item);
                        if (1 < count($parts) && strlen($parts[0])) {
                            $exclude[trim($parts[0])] = array_map(
                                function ($item) {
                                    return trim($item);
                                },
                                explode(',', $parts[1])
                            );
                        } else {
                            $exclude[] = $item;
                        }
                    }
                    break;
            }
        }
        return $this->_excludedUrls = $exclude;
    }

    public function getArrayFromTextArea($string)
    {
        return static::getArrayFromTextArea($string);
    }
    
    public static function getArrayFromTextAreaStatic($string)
    {
        $result = array();
        $items = preg_split('/$\R?^/m', $string);
        foreach ($items as &$item) {
            $item = trim($item);
            if (!is_string($item) || !strlen($item)) {
                continue;
            }
            switch ($item[0])
            {
                case '#':
                    continue;
                    break;
                default:
                    if ('' != $item) {
                        $result[] = $item;
                    }
                    break;
            }
        }
        return $result;
    }

    public function checkForExclusion($url, $patterns, $eventName = '')
    {
        $exclude = false;
        foreach($patterns as $pattern => $patternData) {

            if (is_array($patternData)) {
                $matched = preg_match('/' . $pattern . '/', $url);
                if (strlen($eventName) && $matched && (false !== array_search($eventName, $patternData))) {
                    continue;
                } elseif ($matched) {
                    $exclude = true;
                    break;
                }
            } elseif (preg_match('/' . $patternData . '/', $url)) {
                $exclude = true;
                break;
            }
        }
        return $exclude;
    }

    public function getWritableColumns()
    {
        return array(
            'customer_session_init' => array(
                'customer_session' => array(
                    'id' => self::VAR_SESSION_ID
                    //$observer->getData('customer_session')->getData('_session_validator_data')['remote_addr']
                ),
            ), 'sales_quote_add_item' => array(
                'quote_item' => array(
                    'entity_id' => self::VAR_PRODUCT_ID_ADDED,
                    'remote_ip' => self::VAR_REMOTE_IP,
                    'customer_email' => self::VAR_CUSTOMER_EMAIL,
                    'customer_id' => self::VAR_CUSTOMER_ID,
                    'reserved_order_id' => self::VAR_ORDER_RESERVED_ID
                )
            )
        );
    }

    public function generateWritableColumns($lines, $useEqual = true)
    {
        $wc = array(); 
        $wcEventIndex = -1;
        $wcObjectIndex = -1;
        if (!is_array($lines)) {
            $array = preg_split('/$\R?^/m', $lines);
        } else {
            $array = $lines;
        }
        foreach ($array as &$line) {
            $line = trim($line);
            if (!is_string($line) ||
                is_string($line) && (strlen($line) < 3)) {
                continue;
            }
            switch ($line[0]) {
                // adding new object ot current event
                case '*':
                    if ($wcEventIndex < 0) {
                        $wcObjectIndex = -1;
                        break;
                    }
                    $line = ltrim(ltrim($line, '*'));
                    if (!isset($wc[$line])) {
                        $wc[$wcEventIndex][$line] = array();
                    }
                    $wcObjectIndex = $line; //array_search($line, $wc[$wcEventIndex]);
                    break;
                // adding new datafield to current object
                case '+':
                    if (($wcEventIndex < 0) || ($wcObjectIndex < 0)) {
                        break;
                    }
                    $line = ltrim(ltrim($line, '+'));
                    $lineParts = explode('=', $line);
                    if ((count($lineParts) > 1) && (array_search(trim($lineParts[1]), $this->vc)) && $useEqual) {
                        $wc[$wcEventIndex][$wcObjectIndex][trim($lineParts[0])] = trim($lineParts[1]);
                    } elseif (!$useEqual) {
                        $wc[$wcEventIndex][$wcObjectIndex][] = trim($lineParts[0]);
                    }
                    break;
                // comment, skip
                case '#':
                    ;
                    break;
                // adding new event name
                default:
                    if ('' == $lines) {
                        break;
                    }
                    if (!isset($wc[$line])) {
                        $wc[$line] = array();
                    }
                    $wcEventIndex = $line; //array_search($line, $wc);
                    $wcObjectIndex = -1;
            }
        }
        return $wc;
    }

    public function processDataField($object, $dataField)
    {
        $refObject = $object;
        foreach (explode('.', $dataField) as $field) {
            if (is_object($refObject) && method_exists($refObject, 'getData') && ('' != $field)) {
                $refObject = $refObject->getData($field);
            } else {
                return null;
            }
        }
        return $refObject;
    }

    public function getFormatCell($mask, $vars)
    {
        $result = static::replaceStringByMask($mask, $vars);
        return $result;
    }

    public function getLogFilePath()
    {
        return Mage::getBaseDir('var') . DS . Mage::getStoreConfig('aws_csvlogger/options/log_path');
    }

    public function getLogFileMask()
    {
        return Mage::getStoreConfig('aws_csvlogger/options/log_file_mask');
    }

    public function getConfWritableColumns()
    {
        return Mage::getStoreConfig('aws_csvlogger/options/writable_columns');
    }

    public function isLogDirectoryPrepared()
    {
        $directoryPath = $this->getLogFilePath();
        return is_dir($directoryPath) && is_dir_writeable($directoryPath);
    }

    public function prepareLogDirectory()
    {
        $directoryPath = $this->getLogFilePath();
        if (!is_dir($directoryPath)) {
            return @mkdir($directoryPath, 0770, true) ? $this->isLogDirectoryPrepared() : false;
        } else {
            return file_exists($directoryPath);
        }
    }
    
    public function prepareLogFile()
    {
        if ($this->prepareLogDirectory()) {
            $fileName = rtrim($this->getLogFilePath(), '/\\') . DS . $this->generateLogName();
        }
        return $fileName; // file_exists($fileName) ? $fileName : is_writable($fileName) ? $fileName : false;
    }
    
    public function pushToLog($data)
    {
        if ($path = $this->prepareLogFile()) {
            $fileCSV = new Varien_File_Csv();
            $fh = @fopen($path, 'a');
            $fileCSV->fputcsv($fh, $data);
            return @fclose($fh);
        } else {
            return false;
        }
    }

    /**
     * @param $fileName
     * @param $position
     * @return array of elements
     */
    public function loadLogCSV($fileName, $position)
    {
        $result = '';
        if ($this->isLogDirectoryPrepared()) {
            $path = rtrim($this->getLogFilePath(), '/\\') . DS . $fileName;
            if (file_exists($path)) {
                $file = new SplFileObject($path);
                $file->fseek($position);
                while (!$file->eof()) {
                   // $result[] = $file->fgetcsv();
                    $result .= $file->fgets();
                }
            }
        }
        return $result;
    }

    public function getLogMd5($fileName) {
        $result = '';
        if ($this->isLogDirectoryPrepared()) {
            $path = rtrim($this->getLogFilePath(), '/\\') . DS . $fileName;
            if (file_exists($path)) {
                clearstatcache(true, $path);
                $result = md5_file($path);
            }
        }
        return $result;
    }

    public function getLogSize($fileName)
    {
        $result = 0;
        if ($this->isLogDirectoryPrepared()) {
            $path = rtrim($this->getLogFilePath(), '/\\') . DS . $fileName;
            if (file_exists($path)) {
                clearstatcache(true, $path);
                $result = filesize($path);
            }
        }
        return $result;
    }

    public function getTranslateArray()
    {
        return $this->translated;
    }

    public function getLogFiles()
    {
        if ($this->isLogDirectoryPrepared()) {
            $pattern = preg_replace('/{{(.*?)}}/', '*', $this->getLogFileMask());
            return array_map(function($filePath) {return basename($filePath);}, glob(rtrim($this->getLogFilePath(), '/\\') . DS . $pattern));;
        } else {
            return array();
        }
    }
    
    public function getAdditionalVars()
    {
        $additionalVars = array(
            'dateTime' => date("Y-m-d H:i:s"),
            'current_url' => Mage::app()->getRequest()->getPathInfo(),
            'forwarded_url' => '',
            'session_id' => '',
            'visitor_id' => '',
            'visitor_first_visit_at' => ''
            // Mage::getSingleton('core/url')->parseUrl(Mage::helper('core/url')->getCurrentUrl())->getPath(),
        );
        try {
            if (isset($_SESSION) && isset($_SESSION['core'])) {
                $additionalVars['forwarded_url'] = @$_SESSION['core']['visitor_data']['http_referer'];
                $additionalVars['session_id'] = @$_SESSION['core']['visitor_data']['session_id'];
                $additionalVars['visitor_id'] = @$_SESSION['core']['visitor_data']['visitor_id'];
                $additionalVars['visitor_first_visit_at'] = @$_SESSION['core']['visitor_data']['first_visit_at'];
            }
        } catch (Exception $e) {
            ;
        }
        return $additionalVars;
    }

    public function getStoreConfigValueDirect($path)
    {
        return Mage::getModel('core/config_data')->load($path, 'path')->getValue();
    }

    public function translateByInnerDict($phrase)
    {
        if (0 == count(static::$_dict) && !static::$_dict_loaded) {
            static::$_dict = static::parseKeyValuePair(Mage::getStoreConfig('aws_csvlogger/options/dictionary'));
            foreach (static::$_dict as &$item) {
                $item = str_replace('*nl*', "\n", $item);
            }
            static::$_dict_loaded = true;
        }
        $phrase = str_replace("\n", '*nl*', $phrase);
        if (isset(static::$_dict[$phrase])) {
            $result = @static::$_dict[$phrase];
            return strlen($result) ? $result : $phrase;
        }
        return $phrase;
    }

    public static function getRealPathToSkinFile($pathToFile)
    {
        return @Mage::getDesign()->getFilename($pathToFile, array('_type' => 'skin'));
    }

    /**
     * Render CMS Block by it's ID
     *
     * @param string $cmsBlockId
     * @param array $variables
     * @return string rendered HTML
     */
    public function renderBlock($cmsBlockId, $variables = array())
    {
        return static::renderBlockStatic($cmsBlockId, $variables);
    }

    /**
     * Render CMS Block by it's ID (static version)
     *
     * @param string $cmsBlockId
     * @param array $variables
     * @return string HTML
     */
    public static function renderBlockStatic($cmsBlockId, $variables = array())
    {
        /** @var Mage_Cms_Block_Block $block */
        $block = Mage::getModel('cms/block')->load($cmsBlockId);

        if (!$block->getId()) {
            return '';
        }

        $result = static::renderCustomContent($block->getContent());
        return $result;
    }

    /**
     * Render custom HTML content with variables
     *
     * @param $content
     * @param array $variables
     * @return mixed rendered HTML
     */
    public static function renderCustomContent($content, $variables = array())
    {
        $helper = Mage::helper('cms');
        $processor = $helper->getPageTemplateProcessor();
        $processor->setVariables($variables);
        return $processor->filter($content);
    }

    /**
     *
     *
     * @param string $dest xHTML valid source
     * @param string $source xHTML to append
     * @param string $query selector for append
     * @param string $exportNodeId ID of export note after
     * @return string result xHTML
     */
    public static function injectHtmlCode($dest, $source, $query, $exportNodeId)
    {
        try {
            $dom = new DOMDocument();
            @$dom->loadHTML($dest);

            $xPath = new DOMXPath($dom);
            $results = $xPath->query($query);

            if ($results && isset($results[0])) {
                $frag = $dom->createDocumentFragment();
                $frag->appendXML($source);

                $results[0]->appendChild($dom->importNode($frag, TRUE));
            }

            return $dom->saveHTML($dom->getElementById($exportNodeId));
        } catch (Exception $exception) {}
        return $dest;
    }

    /**
     * Get protected value (from method or property)
     *
     * @param object $object
     * @param string $field field (method or property) value
     * @return mixed|null
     */
    public static function getProtectedField($object, $field)
    {
        try {
            $value = null;
            $reflector = new ReflectionClass(get_class($object));
            if ($reflector->hasMethod($field)) {
                $m = $reflector->getMethod($field);
                $m->setAccessible(true);
                $value = $m->invoke($object);
            } elseif ($reflector->hasProperty($field)) {
                $p = $reflector->getProperty($field);
                $p->setAccessible(true);
                $value = $p->getValue($object);
            }
            return $value;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Set value of protected object's field
     *
     * @param object $object
     * @param string $field
     * @param mixed $value
     * @return null|void
     */
    public static function setProtectedField($object, $field, $value)
    {
        try {
            $reflector = new ReflectionClass(get_class($object));
            if ($reflector->hasProperty($field)) {
                $p = $reflector->getProperty($field);
                $p->setAccessible(true);
                $value = $p->setValue($object, $value);
            }
            return $value;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Set POST/GET parameters
     *
     * @param string $key path to changed data
     * @param mixed $value value to be set
     */
    public static function setRequestParam($key, $value)
    {
        $request = Mage::app()->getRequest();
        $params = $request->getParams();
        $steps = explode('/', $key);
        $mainKey = array_shift($steps);
        if (isset($params[$mainKey])) {
            $buffer = $params[$mainKey];
            $exitFlag = false;
            $buffer = static::_setRecursiveParam($buffer, $steps, $value, $exitFlag);
            if (!$exitFlag) {
                $request->setParam($mainKey, $buffer);
                $request->setPost($mainKey, $buffer);
            }
        }
    }

    /**
     * @param $buffer
     * @param $keys
     * @param $value
     * @param $exitFlag
     * @return array
     */
    protected static function _setRecursiveParam(&$buffer, $keys, $value, &$exitFlag)
    {
        if (is_array($keys) && count($keys)) {
            $key = array_shift($keys);
            $dataType = 'n';
            if (is_array($buffer) && isset($buffer[$key])) {
                $subBuffer = $buffer[$key];
                $dataType = 'a';
            } elseif (is_object($buffer) && method_exists($buffer, 'getData')) {
                $subBuffer = $buffer->getData($key);
                $dataType = 'o';
            } else {
                $exitFlag = true;
                return $buffer;
            }
            $subBuffer = static::_setRecursiveParam($subBuffer, $keys, $value, $exitFlag);
            if (!$exitFlag) {
                try {
                    switch ($dataType) {
                        case 'a':
                            $buffer[$key] = $subBuffer;
                            break;
                        case 'o':
                            $buffer->setData($key, $subBuffer);
                            break;
                    }
                } catch (Exception $exception) {}
            }
        } else {
            return $value;
        }
        return $buffer;
    }

    public static function escapeJavaScriptText($string)
    {
        return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
    }

    public static function escapeCssText($string)
    {
        return str_replace("\n", '', str_replace("\r", '', $string));
    }
}