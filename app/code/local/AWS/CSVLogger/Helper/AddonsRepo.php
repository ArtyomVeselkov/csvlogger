<?php

class AWS_CSVLogger_Helper_AddonsRepo extends Mage_Core_Helper_Abstract
{
    public static $_buffer = array();
    public static $_eventName = '';

    protected static $_removeFromParamsElements = array(
        array(
            '(.+)' => array(
                'form_key' => '0x0',
                'confirmation' => '0x0',
                'password' => '0x0'
            )
        ),
        array(
            '(.+)' => array(
                'login' => array(
                    'password' => '0x0'
                )
            )
        ),
        array(
            '(.+)' => 'payment' // unset element by name
        ),
        array(
            '/\/paypal\/ipn\//' => array(
                'mc_gross' => '0x0', // assign new value
                'business' => '0x0',
                'verify_sign' => '0x0',
                'auth_id' => '0x0',
                'ipn_track_id' => '0x0',
                'receiver_id' => '0x0',
                'receipt_id' => '0x0',
                'payer_id' => '0x0',
                'receiver_email' => '0x0',
                'payer_email' => array('@', '#') // preg_replace in element
            )
        )
    );

    public function getRegisteredAddons()
    {
        return array(
            'logParams' => array(
                'fire_on' => array(
                    'controller_front_init_before'
                ),
                'label' => 'Log $_POST data for each access to site'
            ),
            'uniCustomerLogin' => array(
                'fire_on' => array(
                    'controller_action_predispatch_customer_account_loginpost'
                ),
                'label' => 'Login as customer from the FrontEnd'
            ),
            'uniCustomerLoginFromAdminCallback' => array(
                'fire_on' => array(
                    'adminhtml_widget_container_html_before'
                ),
                'label' => 'Login as customer from the Admin Panel'
            ),
            'validateAndFixCoupon' => array(
                'fire_on' => array(
                    'controller_action_predispatch_checkout_cart_couponpost'
                ),
                'label' => 'Check promotion coupons and fix them if needed'
            ),
            'logPaymentErrors' => array(
                'fire_on' => array(
                    'sales_model_service_quote_submit_failure'
                ),
            'label' => 'Log payment errors, detected by Magento (possibly not all)'
            ),
            'logUserAgent' => array(
                'fire_on' => array(
                    'controller_front_init_before'
                ),
                'label' => 'Log Client\'s User Agent, OS'
            ),
            'execAvsPaymentCheck' => array(
                'fire_on' => array(
                    'sales_order_payment_place_start'
                ),
                'label' => 'Perform AVS Payment Check (PayPal)'
            ),
            'execSaveGiftMessageToOrderNotes' => array(
                'fire_on' => array(
                    'sales_order_place_after'
                ),
                'label' => 'Save gift message to order comments'
            ),
            'execSaveEngravedToOrderNotes' => array(
                'fire_on' => array(
                    'sales_order_place_after'
                ),
                'label' => 'Save engraved options to order comments'
            ),
            'checkShippingRegions' => array(
                'fire_on' => array(
                    'controller_action_predispatch_checkout',
                    'controller_action_layout_render_before'
                ),
                'label' => 'Enable Shipping Restriction On Checkout (backend)'
            ),
            // it should be the last among `controller_action_layout_render_before` to be executed the very last
            'renderLatestStatic' => array(
                'fire_on' => array(
                    'controller_action_layout_render_before'
                ),
                'label' => 'Enable external scripts / styles including'
            )
        );
    }

    public function logUserAgent(&$registers, $eventData = array())
    {
        $result = false;
        $helper = Mage::helper('aws_csvlogger');
        $excludedUrls = Mage::getStoreConfig('aws_csvlogger/detect_ua/extended_version_for_urls');
        $userAgent = getenv('HTTP_USER_AGENT');
        if (
            Mage::getStoreConfig('aws_csvlogger/detect_ua/extended_version_enabled') &&
            // reverse logic -- if true, then use extended version
            $helper->checkForExclusion(Mage::app()->getRequest()->getPathInfo(), $helper->getExcludedUrls($excludedUrls), static::$_eventName)
        ) {
            $result = $this->_logUserAgentExtended($userAgent, $registers, $eventData);
        }
        if (!$result) {
            $registers['detect_ua_result'] = $userAgent;
        } else {
            $registers['detect_ua_result'] = $result;
        }
        return $result;

    }

    protected function _logUserAgentExtendedTotalInfo($result)
    {
        $string = '';
        $string = sprintf('B: \'%s\' (%s) | ', $result->getBrowser()->getName(), $result->getBrowser()->getVersion()->getComplete());
        $string .= sprintf('RE: \'%s\' (%s) | ', $result->getRenderingEngine()->getName(), $result->getBrowser()->getVersion()->getComplete());
        $string .= sprintf('OS: \'%s\' (%s) | ', $result->getRenderingEngine()->getName(), $result->getBrowser()->getVersion()->getComplete());
        $string .= sprintf(
            'Device model %s / brand %s / type %s / mobi %s / touch: %s | ',
            $result->getDevice()->getModel(),
            $result->getDevice()->getBrand(),
            $result->getDevice()->getType(),
            $result->getDevice()->getIsMobile() ? '+' : '-',
            $result->getDevice()->getIsTouch() ? '+' : '-'
        );
        if ($result->getBot()->getIsBot()) {
            $string .= sprintf('Bot: %s (name: \'%s\', type: \'%s\')',
                $result->getBot()->isBot() ? '+' : '-',
                $result->getBot()->getName(),
                $result->getBot()->getType()
            );
        } else {
            $string .= sprintf('Not a bot');
        }
        return $string;
    }

    protected function _logUserAgentExtended($userAgentString, &$registers, $eventData = array())
    {
        // load 3d party
        $pathToComposerAutoloader = Mage::getBaseDir('lib');
        $pathToComposerAutoloader .= '/AWS/vendor/autoload.php';
        $curDir = getcwd();
        try {
            if (file_exists($pathToComposerAutoloader) && (chdir(dirname($pathToComposerAutoloader) . '/..'))) {
                @require_once($pathToComposerAutoloader);

                $chain = new UserAgentParser\Provider\Chain([
                    //new UserAgentParser\Provider\PiwikDeviceDetector(new DeviceDetector\DeviceDetector),
                    //new UserAgentParser\Provider\WhichBrowser(new WhichBrowser\Parser()),
                    //new UserAgentParser\Provider\UAParser(UAParser\Parser::create()),
                    new UserAgentParser\Provider\PiwikDeviceDetector(),
                    new UserAgentParser\Provider\WhichBrowser(),
                    new UserAgentParser\Provider\UAParser(),
                    new UserAgentParser\Provider\Woothee(),
                    new UserAgentParser\Provider\DonatjUAParser()
                ]);

                $result = $chain->parse(
                    $userAgentString,
                    Mage::getStoreConfig('aws_csvlogger/detect_ua/extended_collect_additional_data') ? getallheaders() : null
                );

                return $this->_logUserAgentExtendedTotalInfo($result);
            }
        } catch (Exception $e) {
            chdir($curDir);
            return false;
        }
    }

    public function logParams(&$registers, $eventData = array())
    {
        $params = Mage::app()->getRequest()->getParams();
        $uri = Mage::app()->getRequest()->getRequestUri();
        // uri checking
        foreach (static::$_removeFromParamsElements as $checkItem) {
            foreach ($checkItem as $uriCheck => $dataCheck) {
                if (preg_match($uriCheck, $uri)) {
                    try {
                        if (is_string($dataCheck) && isset($params[$dataCheck])) {
                            unset($params[$dataCheck]);
                        } elseif (is_array($dataCheck)) {
                            // sub-level replacement
                            foreach ($dataCheck as $key => $value) {
                                if (isset($params[$key])) {
                                    if (is_array($value) && is_array($params[$key])) {
                                        // in key replacement
                                        if (
                                            is_array($value) && 2 == count($value) && isset($value[0]) && isset($value[0]) &&
                                            is_string($value[0]) && is_string($value[1])
                                        ) {
                                            $params[$key] = preg_replace('/' . @$value[0] . '/', @$value[1], $params[$key]);
                                        } else {
                                            foreach ($value as $from => $to) {
                                                if (is_array($to) && 2 == count($to) && isset($params[$key][$from])) {
                                                    $params[$key][$from] = preg_replace('/' . @$to[0] . '/', @$to[1], $params[$key][$from]);
                                                } elseif (is_string($to) && isset($params[$key][$from])) {
                                                    $params[$key][$from] = $to;
                                                }
                                            }
                                        }
                                    } elseif (is_string($value)) {
                                        $params[$key] = $value;
                                    }
                                }
                            }
                        }
                    } catch (\Exception $exception) {
                        ;
                    }
                }
            }
        }
        $registers['params'] = print_r($params, true);
    }

    public function validateAndFixCoupon(&$registers, $eventData = array())
    {
        $couponCode = Mage::app()->getRequest()->getParam('coupon_code');
        $registers['coupon_code_origin'] = $couponCode;
        if (strlen($couponCode)) {
            $sanitizedCouponCode = preg_replace("/[^\w]+/", "", $couponCode);
            if ($sanitizedCouponCode != $couponCode) {
                $couponsCollection = Mage::getModel('salesrule/coupon')->getCollection();
                foreach($couponsCollection as $coupon){
                    $couponCodeItem = $coupon->getCode();
                    if ($couponCodeItem == $couponCode) {
                        # coupon is right
                        $registers['coupon_code_info'] = 'right coupon code';
                        return;
                    } elseif ($couponCodeItem == $sanitizedCouponCode) {
                        # coupon is not right, fixing it
                        # may be enter some message about it
                        Mage::app()->getRequest()->setParam('coupon_code', $sanitizedCouponCode);
                        $registers['coupon_code_info'] = 'coupon has wrong chars, fixed';
                        $registers['coupon_code_fixed'] = $sanitizedCouponCode;
                        return;
                    }
                }
            } else {
                $registers['coupon_code_info'] = 'coupon code syntax is valid, not checked';
                return;
            }
            # coupon is wrong - leave all the same
            $registers['coupon_code_info'] = 'wrong coupon code';
        }
    }

    public function uniCustomerLogin(&$registers, $eventData = array())
    {
        $step = 1;
        $registers['unilogin_log'] = "UniLogin: ($step) initiated";
        $request = Mage::app()->getRequest();
        $session = Mage::getSingleton('customer/session');

        $helper = Mage::helper('aws_csvlogger');
        $helperAddon = Mage::helper('aws_csvlogger/addons');
        $allowedIP = $helper->getArrayFromTextArea(Mage::getStoreConfig('aws_csvlogger/unilogin_options/unilogin_allowed_ip'));
        $enabled = Mage::getStoreConfig('aws_csvlogger/unilogin_options/unilogin_enable') && $helperAddon->isAddonEnabled();

        $ip = $_SERVER['REMOTE_ADDR'];

        $step++;
        if (!Mage::getSingleton('customer/session')->isLoggedIn() && $request->isPost() && $enabled &&  in_array($ip, $allowedIP)) {
            $login = $request->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $email = '';
                    if ($this->_uniCustomerLogin($login['username'], $login['password'], $email)) {
                        $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                            ->loadByEmail($email);

                        Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer)
                            ->renewSession();

                        Mage::app()->getResponse()->setRedirect(Mage::helper('customer')->getDashboardUrl());
                        $registers['unilogin_log'] .= " ($step) successful login, proceed.";
                    } else {
                        $registers['unilogin_log'] .= " ($step) access denied - wrong credentials, canceled.";
                    }
                } catch (Exception $e) {
                    $registers['unilogin_log'] .= " ($step) error occurred while performing login (\"" . $e->getMessage() . '"), canceled';
                }
            } else {
                $registers['unilogin_log'] .= " ($step) empty login or password, canceled.";
            }
        } else {
            $registers['unilogin_log'] .= " ($step) bad attempt to login, access denied!";
        }
    }

    public function uniCustomerLoginFromAdminCallback(&$registers, $eventData = array())
    {
        $helperAddon = Mage::helper('aws_csvlogger/addons');
        if (
            !Mage::getStoreConfig('aws_csvlogger/unilogin_options/unilogin_enable_from_admin') ||
            !$helperAddon->isAddonEnabled('uniCustomerLoginFromAdminCallback')
        ) {
            return $this;
        }

        // add button "Login As Customer"
        $block = isset($eventData['block']) ? $eventData['block'] : null;
        if (is_a($block, 'Mage_Adminhtml_Block_Customer_Edit')) {
            $customerId = $this->_getCustomerIdByRegistry();
            if ($customerId) {
                $url = Mage::getModel('adminhtml/url')->getUrl(
                    '*/logger/addon/',
                    array(
                        'addon_action' => 'uniCustomerLoginFromAdminForward',
                        'from_frontend' => '1',
                        'customer_id' => $customerId,
                        // seems to be no needed
                        // '_cache_secret_key', true /* NB-547 */
                    )
                );

                $level     = 0;
                $sortOrder = -1;
                $area      = 'header';
                $block->addButton('aws_csvlogger_addon_loginas_btn',
                    array(
                        'label'   => Mage::helper('aws_csvlogger')->__('Login As Customer'),
                        'onclick' => "window.open('$url')",
                    ), $level, $sortOrder, $area);
            }
        }
        return $this;
    }

    protected function _uniCustomerLogin($login, $password, &$email)
    {
        $adminPrefixSecurity = Mage::getStoreConfig('aws_csvlogger/unilogin_options/unilogin_password_prefix');
        if (!is_string($adminPrefixSecurity) || (strlen($adminPrefixSecurity) < 2)) {
            return false;
        }

        $prefixLogin = substr($login, 0, strpos($login, '%'));
        if ($prefixLogin != 'unilogin') {
            return false;
        }
        $email = substr($login, strpos($login, '%') + 1, strlen($login));

        $prefixPassword = substr($password, 0, strpos($password, '%'));
        $postFixPassword = substr($password, strpos($password, '%') + 1, strpos($password, '#') - strpos($password, '%') - 1);
        $lastfixPassword = substr($password, strpos($password, '#'), strlen($password));


        if ($prefixPassword != $adminPrefixSecurity || $postFixPassword != date('j') || $lastfixPassword != '#') {
            return false;
        }

        $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email);

        if (!is_null($customer) && ($customer->getId()) && $customer->getEmail() == $email) {
            return true;
        }
    }

    // TODO Ability temporary replace customer address by another tester's one (+ column, event, crontab)
    /**
     * @param array $params array of parameters send by POST method
     * @return bool|array if non FALSE value -- redirect info array('url' => 'string', 'params' => array())
     */
    public function uniCustomerLoginFromAdminForward(array $params)
    {
        $helperAddon = Mage::helper('aws_csvlogger/addons');
        if (
            !Mage::getStoreConfig('aws_csvlogger/unilogin_options/unilogin_enable_from_admin') ||
            !$helperAddon->isAddonEnabled('uniCustomerLoginFromAdminCallback') ||
            !count($params) || !isset($params['customer_id'])
        ) {
            return false;
        }
        $customerId = $params['customer_id'];
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customerSession = Mage::getSingleton('customer/session');
        $currentSessionCustomerId = $customerSession->getCustomerId();
        if (!$customer->getId()) {
            return false;
        }

        Mage::dispatchEvent('aws_csvloggger_addon_unilogin_admin_before_login');

        if ($currentSessionCustomerId)
        {
            if ($currentSessionCustomerId != $customerId) {
                Mage::getSingleton('checkout/session')->clear();
                Mage::getSingleton('catalog/session')->clear();
                // in order not to be thrown away from current admin page
                // Mage::getSingleton('core/session')->clear();
                Mage::getSingleton('customer/session')->clear();
                Mage::getSingleton('newsletter/session')->clear();
                Mage::getSingleton('paypal/session')->clear();
                Mage::getSingleton('paypal/session')->clear();
                Mage::getSingleton('reports/session')->clear();
                Mage::getSingleton('review/session')->clear();
                Mage::getSingleton('wishlist/session')->clear();
                Mage::getSingleton('catalogsearch/session')->clear();
                Mage::getSingleton('paypaluk/session')->clear();

                $persistentSession = Mage::getSingleton('persistent/session');
                if ($persistentSession) {
                    try {
                        $persistentSession
                            ->clearInstance()
                            ->deleteByCustomerId($customerSession->getCustomerId());
                    } catch (Exception $e) {
                        Mage::log('CSVLogger > Addon > Error while clearing persistent session (' . $e->getMessage() . ')', null, 'csvlogger.log');
                    }
                }
            }
        }

        if (method_exists($customerSession, 'renewSession')) {
            $customerSession->renewSession();
        } else // for 1.4
        {
            $customerSession->logout();
        }

        $customerSession->loginById($customerId);

        $result = array(
            'url' => 'customer/account/',
            'params' => array(
                '_store' => $this->_getCustomerStoreId($customerId)
            )
        );

        return $result;
    }

    protected function _getCustomerIdByRegistry()
    {
        $customerId  = 0;
        $currentCustomer = Mage::registry('current_customer');
        $currentOrder = Mage::registry('current_order');

        if (is_a($currentCustomer, 'Mage_Customer_Model_Customer')) {
            $customerId = $currentCustomer->getId();
        } elseif (is_a($currentOrder, 'Mage_Sales_Model_Order')) {
            $customerId = $currentOrder->getCustomerId();
        } else {
            $customerId = 0;
        }

        return $customerId;
    }

    protected function _getCustomerStoreId($customerId)
    {
        if (!$customerId)
        {
            return false;
        }
        $customer = Mage::getModel('customer/customer')->load($customerId);
        if ($customer->getStoreId())
        {
            $customerStore = Mage::app()->getStore($customer->getStoreId());
            if ($customerStore->getId() && $customerStore->getIsActive())
            {
                return $customer->getStoreId();
            }
        }
        if ($customer->getWebsiteId())
        {
            $customerWebsite = Mage::app()->getWebsite($customer->getWebsiteId());
            foreach ($customerWebsite->getStores() as $websiteStore)
            {
                if ($websiteStore->getIsActive())
                {
                    return $websiteStore->getId();
                }
            }
        }
        if (0 == Mage::getStoreConfig('customer/account_share/scope'))
        {
            return Mage::app()->getDefaultStoreView()->getId();
        }
        return false;
    }

    protected function _isUniLoginAllowed($customerId) {
        $adminSession = Mage::getSingleton('admin/session');
        $transport    = new Varien_Object(
            array('disable' => false)
        );
        Mage::dispatchEvent('aws_csvlogger_addon_loginas_allowed', array(
            'transport'   => $transport,
            'customer_id' => $customerId,
        ));
        return ($adminSession->isAllowed('unilogin_enable_from_admin') || $transport->getDisable());
    }

    public function logPaymentErrors(&$registers, $eventData = array())
    {
        // hack
        $useMethod = 2;

        $helperAddon = Mage::helper('aws_csvlogger/addons');
        switch ($useMethod) {
            // here were more methods early ...
            case 2:
            default:
                $helperAddon->addRuntimeAddon('logPaymentErrorsHookPostD', array(
                    'fire_on' => array(
                        'controller_action_postdispatch_checkout_onepage_saveorder'
                    ),
                    'label' => 'Hook for getting payment error message (postdispatch)'
                ));
                break;
        }
        $registers['payment_place_order_error'] = 'Some error occurred';
        static::$_buffer['payment_error'] = true;

        return $this;
    }

    public function logPaymentErrorsHookPostD(&$registers, $eventData = array())
    {
        if (isset(static::$_buffer['payment_error'])  && static::$_buffer['payment_error']) {
            $jsonData = Mage::app()->getResponse()->getBody(false);
            $arrayData = json_decode($jsonData, true);
            $errorMsg = @$arrayData['error_messages'];
            $registers['payment_place_order_error'] = sprintf('%s', $errorMsg);
            static::$_buffer['payment_error'] = false;
        }
    }

    /**
     *
     * @event sales_order_payment_place_start
     */
    public function execAvsPaymentCheck(&$registers, $eventData = array())
    {
        $enabled = Mage::getStoreConfig('aws_csvlogger/payment_fraude/enabled');

        if (is_array($eventData) && is_a(@$eventData['payment'], 'Mage_Sales_Model_Order_Payment') && $enabled) {
            $rulesLines = Mage::getStoreConfig('aws_csvlogger/payment_fraude/rules');
            /** @var Aws_CSVLogger_Helper_Data $helper */
            $helper = Mage::helper('aws_csvlogger');
            $rules = $helper->generateWritableColumns($rulesLines, false);

            $payment = $this->execAvsPaymentCheckClonePayment($eventData['payment']);
            $result = $this->execAvsPaymentCheckCall($payment);

            if (is_array($result)) {
                $registers['payment_avs_result'] = @$result['avs_result'];
                $registers['payment_csc_result'] = @$result['cvv2_check_result'];
                $registers['payment_fraud_result'] = @$result['fraud'];
                $registers['payment_txn_result'] = @$result['transaction_id'];
                $registers['payment_fraud_method'] = @$result['payment_method'];
            }

            $selectedRules = static::execAvsPaymentCheckPrepareRulesByMethod($rules, @$result['payment_method']);
            if (is_array($result) && is_array($selectedRules) && 0 < count($selectedRules)) {
                $blockingResult = $this->execAvsPaymentCheckCallPerformValidation($result, $selectedRules);
                if (!@$blockingResult['avs']) {
                    return array(
                        'error' => $helper->translateByInnerDict("Sorry, order can't be completed. \nCredit Card used for payment must match Billing Address. \nPlease try again.")
                    );
                }
                if (!@$blockingResult['csc']) {
                    return array(
                        'error' => $helper->translateByInnerDict("Sorry, order can't be completed. \nInvalid CVV code. \nPlease try again.")
                    );
                }
            }

        }
        return array();
    }

    protected static function execAvsPaymentCheckPrepareRulesByMethod($rules, $methodCode)
    {
        if (isset($rules[$methodCode])) {
            return $rules[$methodCode];
        }
        return array();
    }


    protected function execAvsPaymentCheckClonePayment(Mage_Sales_Model_Order_Payment $payment)
    {
        if (!$payment) {
            return null;
        }
        $result = clone $payment;
        $result->setId(null);
        $result->setCreatedAt(null);
        $result->setUpdatedAt(null);

        $originalOrder = $payment->getOrder();
        $order = clone $originalOrder;

        $order->setId(null);

        $order->setBaseGrandTotal(0.0);
        $order->setBaseSubtotal(0.0);
        $order->setBaseTaxAmount(0.0);
        $order->setGrandTotal(0.0);
        $order->setSubtotal(0.0);
        $order->setTaxAmount(0.0);
        $order->setBaseSubtotalInclTax(0.0);
        $order->setSubtotalInclTax(0.0);
        $order->setTotalItemCount(0.0);

        $result->setOrder($order);

        return $result;
    }

    protected function execAvsPaymentCheckGetConfig()
    {
        return false !== Mage::getConfig()->getNode('modules/Mage_Centinel') && 1 == Mage::getStoreConfig('payment//centinel');
    }

    protected function execAvsPaymentCheckCall(Mage_Sales_Model_Order_Payment $payment)
    {
        if (is_a($payment->getMethodInstance(), 'Mage_Paypal_Model_Direct')) {
            return $this->execAvsPaymentCheckCallPayPal($payment);
        }
        return false;
    }

    protected function execAvsPaymentCheckCallPayPal(Mage_Sales_Model_Order_Payment $paymentPayPal)
    {
        $order = $paymentPayPal->getOrder();
        /** @var Mage_Paypal_Model_Pro $pro */
        $pro = Mage::getModel('paypal/pro');
        $config = Mage::getModel('paypal/config');
        $config->setMethod($paymentPayPal->getMethod());
        $config->setStoreId($order->getStoreId());
        $pro->setConfig($config);
        /** @var Mage_Paypal_Model_Api_Nvp $api */
        $api = $pro->getApi()
            ->setPaymentAction('Authorization')
            ->setIpAddress(Mage::app()->getRequest()->getClientIp(false))
            ->setAmount(0.0)
            ->setCurrencyCode($order->getBaseCurrencyCode())
            ->setEmail($order->getCustomerEmail())
            ->setNotifyUrl(Mage::getUrl('paypal/ipn/', array('_store' => $order->getStore()->getStoreId())))
            ->setCreditCardType($paymentPayPal->getCcType())
            ->setCreditCardNumber($paymentPayPal->getCcNumber())
            ->setCreditCardExpirationDate(
                sprintf('%02d%02d', $paymentPayPal->getCcExpMonth(), $paymentPayPal->getCcExpYear())
            )
            ->setCreditCardCvv2($paymentPayPal->getCcCid())
            ->setMaestroSoloIssueNumber($paymentPayPal->getCcSsIssue())
        ;
        if ($paymentPayPal->getCcSsStartMonth() && $paymentPayPal->getCcSsStartYear()) {
            $year = sprintf('%02d', substr($paymentPayPal->getCcSsStartYear(), -2, 2));
            $api->setMaestroSoloIssueDate(
                sprintf('%02d%02d', $paymentPayPal->getCcSsStartMonth(), $year)
            );
        }
        // we don't use here 3D S

        // add shipping and billing addresses
        if ($order->getIsVirtual()) {
            $api->setAddress($order->getBillingAddress())->setSuppressShipping(true);
        } else {
            $api->setAddress($order->getShippingAddress());
            $api->setBillingAddress($order->getBillingAddress());
        }

        try {
            $api->callDoDirectPayment();
        } catch (\Exception $exception) {
            ;
        }
        $paymentPayPal->setTransactionId($api->getTransactionId())->setIsTransactionClosed(0);
        $pro->importPaymentInfo($api, $paymentPayPal);

        $resultData = [
            'avs_result' => $api->getAvsResult(),
            'cvv2_check_result' => $api->getCvv2CheckResult(),
            'transaction_id' => $api->getTransactionId(),
            'fraud' => $api->getIsFraudDetected(),
            'payment_method' => $paymentPayPal->getMethod()
        ];

        return $resultData;
    }

    protected function execAvsPaymentCheckCallPerformValidation(array $data, array $rules)
    {
        $result = [
            'avs' => true,
            'csc' => true,
            'txn' => isset($data['transaction_id']) ? $data['transaction_id'] : 0,
            'fraud' => isset($data['fraud']) ? $data['fraud'] : 0,
            'payment_method' => isset($data['payment_method']) ? $data['payment_method'] : '<undefined>'
        ];

        $avsResult = @$data['avs_result'];
        $cscResult = @$data['cvv2_check_result'];

        if (isset($rules['avs']) && is_array(@$rules['avs'])) {
            $result['avs'] = in_array($avsResult, $rules['avs']);
        }
        if (isset($rules['csc']) && is_array(@$rules['csc'])) {
            $result['csc'] = in_array($cscResult, $rules['csc']);
        }

        return $result;
    }


    /**
     * @param $registers
     * @param array $eventData
     * @return array
     */
    public function execSaveGiftMessageToOrderNotes(&$registers, $eventData = array())
    {
        $result = array();
        $giftMessages = array();
        $settings = array();
        $skipFlag = false;
        try {
            $settings = $this->execSaveGiftMessageToOrderNotesGetConfigs();
            /** @var Mage_Sales_Model_Order */
            $order = @$eventData['order'];
            if (is_a($order, 'Mage_Sales_Model_Order')) {
                $orderItems = $order->getAllItems();
                if (is_array($orderItems)) {
                    foreach ($orderItems as $orderItem) {
                        $buffer = $this->execSaveGiftMessageToOrderNotesProcessOrderItem($orderItem);
                        if (is_array($buffer) && !empty($buffer) && true === @$buffer['message_exists_flag']) {
                            $buffer['order_id'] = @$order->getId();
                            $buffer['order_increment'] = @$order->getIncrementId();
                            $buffer['customer_email'] = @$order->getCustomerEmail();
                            $giftMessages[] = $buffer;
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $messageOutside = 'Not able to add gift messages as a comment';
            if (isset($registers['payment_place_order_error'])) {
                $registers['payment_place_order_error'] .= '; ' . $messageOutside;
            } else {
                $registers['payment_place_order_error'] = $messageOutside;
            }
            $skipFlag = true;
        }
        if (!$skipFlag) {
            $comment = $this->execSaveMessageToOrderNotesPrepareToHistory($giftMessages, $settings);
            if ((is_string($comment) && strlen($comment)) || (is_array($comment) && !empty($comment))) {
                $this->execSaveMessageToOrderNotesAddStatusHistoryComment($order, $comment, $settings);
                $messageOutside = 'Converted GMs to order comment: ' . $comment;
                if (isset($registers['additional_info'])) {
                    $registers['additional_info'] .= '; ' . $messageOutside;
                } else {
                    $registers['additional_info'] = $messageOutside;
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public function execSaveGiftMessageToOrderNotesGetConfigs()
    {
        $startCommentWith = Mage::getStoreConfig('aws_csvlogger/gift_message_to_order/start_comment_with');
        if (!is_string($startCommentWith) || !strlen($startCommentWith)) {
            $startCommentWith = "Gift Message: \r\n";
        }
        $endCommentWith = Mage::getStoreConfig('aws_csvlogger/gift_message_to_order/end_comment_with');
        if (!is_string($endCommentWith) || !strlen($endCommentWith)) {
            $endCommentWith = "";
        }
        $orderedItemsMask = Mage::getStoreConfig('aws_csvlogger/gift_message_to_order/ordered_item_mask');
        if (!is_string($orderedItemsMask) || 9 > strlen($orderedItemsMask)) {
            $orderedItemsMask = '{{product_name}} x {{product_qty}} | TO: {{message_to}} FROM: {{message_from}} CONTENT: {{message_content}}';
        }
        $delimiterRaw = Mage::getStoreConfig('aws_csvlogger/gift_message_to_order/delimiter_for_items');
        if (!is_string($delimiterRaw) || !strlen($delimiterRaw)) {
            $delimiter = " \r\n";
        } else {
            $from = array('\\r', '\\n', '\\t');
            $to = array("\r", "\n", "\t");
            $delimiter = str_replace($from, $to, $delimiterRaw);
        }
        $appendTypeRaw = Mage::getStoreConfig('aws_csvlogger/gift_message_to_order/notify_types');
        if (!is_string($appendTypeRaw) || !strlen($appendTypeRaw)) {
            $appendType = array('separate', 'split_by_item');
        } else {
            $appendType = explode(',', $appendTypeRaw);
        }
        return array(
            'mask' => $orderedItemsMask,
            'delimiter' => $delimiter,
            'start' => $startCommentWith,
            'end' => $endCommentWith,
            'append_type' => $appendType
        );
    }

    public function execSaveGiftMessageToOrderNotesNotifyTypes()
    {
        return array(
            array(
                'label' => 'Add as a separate comment',
                'value' => 'separate'
            ),
            array(
                'label' => 'Append to customer\'s notes',
                'value' => 'customer_note'
            ),
            array(
                'label' => 'If a separate comment -- split by one per item',
                'value' => 'split_by_item'
            )
        );
    }

    /**
     * @param $orderItem
     * @return array
     */
    protected function execSaveGiftMessageToOrderNotesProcessOrderItem($orderItem)
    {
        $skipFlag = false;
        try {
            $result = array(
                'product_name' => '',
                'message_content' => '',
                'message_from' => '',
                'message_to' => '',
                'message_exists_flag' => false
            );
            $giftMessage = Mage::helper('giftmessage/message')->getGiftMessage(
                @$orderItem->getGiftMessageId()
            );
            if (is_object($giftMessage) && $giftMessage->getId()) {
                /** @var Mage_Sales_Model_Order_Item $orderItem */
                $result = array(
                    'product_name' => $orderItem->getName(),
                    'product_sku' => $orderItem->getSku(),
                    'product_qty' => $orderItem->getQtyOrdered(),
                    'message_content' => $giftMessage->getMessage(),
                    'message_from' => $giftMessage->getSender(),
                    'message_to' => $giftMessage->getRecipient(),
                    'message_exists_flag' => true
                );
            }
        } catch (\Exception $exception) {
            $result['message_exists_flag'] = false;
            $skipFlag = true;
        }
        if (!$skipFlag) {
            $result['message_exists_flag'] = true;
        }
        return $result;
    }

    /**
     * @param $giftMessages
     * @param $settings
     * @return string
     */
    protected function execSaveMessageToOrderNotesPrepareToHistory($giftMessages, $settings)
    {
        $implodedResult = '';
        if (is_array($giftMessages) && count($giftMessages)) {
            $preparedCommentArray = array();
            foreach ($giftMessages as $giftMessage) {
                $buffer = AWS_CSVLogger_Helper_Data::replaceStringByMask(
                    $settings['mask'],
                    $giftMessage
                );
                if (strlen($buffer)) {
                    $preparedCommentArray[] = $buffer;
                }
                $buffer = '';
            }
            if (!@in_array('split_by_item', $settings['append_type'])) {
                $implodedResult = implode($settings['delimiter'], $preparedCommentArray);
                if (is_string($implodedResult) && strlen($implodedResult)) {
                    $implodedResult = @$settings['start'] . $implodedResult . @$settings['end'];
                }
            } else {
                $implodedResult = array();
                foreach ($preparedCommentArray as $preparedCommentItem) {
                    $implodedResult[] = @$settings['start'] . $preparedCommentItem . @$settings['end'];
                }
            }
        }
        return $implodedResult;
    }

    protected function execSaveMessageToOrderNotesPrepareToHistoryPreProcess($messages, $settings, $field = 'options')
    {
        $mainMask = @$settings['submask'];
        $subPrefix = @$settings['subprefix'];
        $prefix = @$settings['prefix'];
        $delimiter = @$settings['subdelimiter'];
        $result = array();
        if (is_string($mainMask) && strlen($mainMask) && is_array($messages)) {
            foreach ($messages as $message) {
                if (is_array($message) && isset($message[$field]) && count($message[$field])) {
                    $bufferStandard = $message;
                    try {
                        unset($bufferStandard[$field]);
                    } catch (\Exception $exception) {
                        ;
                    }
                    $bufferStandard['message_content'] = '';
                    $increment = 0;
                    $resultOptions = array();
                    foreach ($message[$field] as $optionItem) {
                        $increment++;
                        $buffer = $bufferStandard;
                        $buffer = array_merge($buffer, $optionItem);
                        $subBuffer = AWS_CSVLogger_Helper_Data::replaceStringByMask(
                            $mainMask,
                            $buffer
                        );
                        switch ($subPrefix) {
                            case 'ol':
                                $prefixCurrent = $increment . ') ';
                                break;
                            default:
                                $prefixCurrent = $subPrefix;
                                break;
                        }
                        $resultOptions[] = @$prefix . @$prefixCurrent . @$subBuffer;
                    }
                    if (count($resultOptions)) {
                        $bufferStandard['message_content'] = @implode($delimiter, $resultOptions);
                    }
                    $result[] = $bufferStandard;
                }
            }
        }
        return $result;
    }

    protected function execSaveMessageToOrderNotesAddStatusHistoryComment($order, $comment, $settings, $status = false)
    {
        /** @see app/code/core/Mage/Sales/Model/Order.php:1067 */
        try {
            if (!is_a($order, 'Mage_Sales_Model_Order')) {
                return false;
            }
            if (false === $status) {
                $status = $order->getStatus();
            } elseif (true === $status) {
                $status = $order->getConfig()->getStateDefaultStatus($order->getState());
            } else {
                $order->setStatus($status);
            }
            $appendToCustomerNotes = @in_array('customer_note', $settings['append_type']);
            $addAsHistoryNote = @in_array('separate', $settings['append_type']);
            if ($addAsHistoryNote) {
                if (is_string($comment)) {
                    $comment = array($comment);
                }
                if (is_array($comment)) {
                    foreach ($comment as $commentItem) {
                        try {
                            if (is_string($commentItem) && strlen($commentItem)) {
                                $history = Mage::getModel('sales/order_status_history')
                                    ->setStatus($status)
                                    ->setComment($commentItem)
                                    ->setEntityName(Mage_Sales_Model_Order::HISTORY_ENTITY_NAME)
                                    ->setIsCustomerNotified(true);
                                $order->addStatusHistory($history);
                            }
                        } catch (\Exception $exception) {
                            ;
                        }
                    }
                }
            }
            if ($appendToCustomerNotes) {
                $customerNotes = @$order->getCustomerComment();
                if (is_string($customerNotes) && strlen($customerNotes)) {
                    $customerNotes .= "\r\n\r\n" . $comment;
                } else {
                    $customerNotes = $comment;
                }
                @$order->setCustomerComment($customerNotes);
                $customerNotes = @$order->getCustomerNotes();
                if (is_string($customerNotes) && strlen($customerNotes)) {
                    $customerNotes .= "\r\n\r\n" . $comment;
                } else {
                    $customerNotes = $comment;
                }
                @$order->setCustomerNotes($customerNotes);
            }
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function execSaveEngravedToOrderNotes(&$registers, $eventData = array())
    {
        $result = array();
        $messages = array();
        $settings = array();
        $skipFlag = false;
        try {
            $settings = $this->execSaveEngravedToOrderNotesGetConfigs();
            /** @var Mage_Sales_Model_Order */
            $order = @$eventData['order'];
            if (is_a($order, 'Mage_Sales_Model_Order')) {
                $orderItems = $order->getAllItems();
                if (is_array($orderItems)) {
                    foreach ($orderItems as $orderItem) {
                        $buffer = $this->execSaveEngravedToOrderNotesProcessOrderItem($orderItem);
                        if (is_array($buffer) && !empty($buffer) && true === @$buffer['message_exists_flag']) {
                            $buffer['order_id'] = @$order->getId();
                            $buffer['order_increment'] = @$order->getIncrementId();
                            $buffer['customer_email'] = @$order->getCustomerEmail();
                            $messages[] = $buffer;
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $messageOutside = 'Not able to add custom options as a comment';
            if (isset($registers['payment_place_order_error'])) {
                $registers['payment_place_order_error'] .= '; ' . $messageOutside;
            } else {
                $registers['payment_place_order_error'] = $messageOutside;
            }
            $skipFlag = true;
        }
        if (!$skipFlag) {
            $preProcessedMessages = $this->execSaveMessageToOrderNotesPrepareToHistoryPreProcess($messages, $settings, 'options');
            $comment = $this->execSaveMessageToOrderNotesPrepareToHistory($preProcessedMessages, $settings);
            if ((is_string($comment) && strlen($comment)) || (is_array($comment) && !empty($comment))) {
                $this->execSaveMessageToOrderNotesAddStatusHistoryComment($order, $comment, $settings);
                $messageOutside = 'Converted Custom Options to order comment: ' . $comment;
                if (isset($registers['additional_info'])) {
                    $registers['additional_info'] .= '; ' . $messageOutside;
                } else {
                    $registers['additional_info'] = $messageOutside;
                }
            }
        }
        return $result;
    }

    public static function prepareRTNString($string)
    {
        if (is_string($string)) {
            $from = array('\\r', '\\n', '\\t');
            $to = array("\r", "\n", "\t");
            return @str_replace($from, $to, $string);
        } else {
            return $string;
        }
    }

    /**
     * @return array
     */
    public function execSaveEngravedToOrderNotesGetConfigs()
    {
        $startCommentWith = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/start_comment_with');
        if (!is_string($startCommentWith) || !strlen($startCommentWith)) {
            $startCommentWith = "** Custom Options **\r\n";
        }
        $endCommentWith = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/end_comment_with');
        if (!is_string($endCommentWith) || !strlen($endCommentWith)) {
            $endCommentWith = "";
        }
        $orderedItemsMask = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/ordered_item_mask');
        if (!is_string($orderedItemsMask) || 9 > strlen($orderedItemsMask)) {
            $orderedItemsMask = "{{product_name}} x {{product_qty}}\r\n{{message_content}}";
        }
        $delimiterRaw = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/delimiter_for_items');
        if (!is_string($delimiterRaw) || !strlen($delimiterRaw)) {
            $delimiter = " \r\n";
        } else {
            $delimiter = static::prepareRTNString($delimiterRaw);
        }
        $appendTypeRaw = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/notify_types');
        if (!is_string($appendTypeRaw) || !strlen($appendTypeRaw)) {
            $appendType = array('separate', 'split_by_item');
        } else {
            $appendType = explode(',', $appendTypeRaw);
        }
        $subPrefix = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/subprefix');
        if (!is_string($subPrefix)) {
            $subPrefix = 'ol';
        }
        $subPrefix = static::prepareRTNString($subPrefix);
        $prefix = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/prefix');
        if (!is_string($prefix)) {
            $prefix = "\t";
        }
        $prefix = static::prepareRTNString($prefix);
        $subMask = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/submask');
        if (!is_string($subMask) || 9 > strlen($subMask)) {
            $subMask = '\t{{label}}: {{value}}';
        }
        $subMask = static::prepareRTNString($subMask);
        $subDelimiterRaw = Mage::getStoreConfig('aws_csvlogger/custom_options_to_order_notes/subdelimiter_for_items');
        if (!is_string($subDelimiterRaw) || !strlen($subDelimiterRaw)) {
            $subDelimiter = " \r\n";
        } else {
            $subDelimiter = static::prepareRTNString($subDelimiterRaw);
        }
        return array(
            'mask' => $orderedItemsMask,
            'delimiter' => $delimiter,
            'start' => $startCommentWith,
            'end' => $endCommentWith,
            'append_type' => $appendType,
            'submask' => $subMask,
            'subdelimiter' => $subDelimiter,
            'subprefix' => $subPrefix,
            'prefix' => $prefix
        );
    }

    public function execSaveEngravedToOrderNotesNotifyTypes()
    {
        return array(
            array(
                'label' => 'Add as a separate comment',
                'value' => 'separate'
            ),
            array(
                'label' => 'Append to customer\'s notes',
                'value' => 'customer_note'
            ),
            array(
                'label' => 'If a separate comment -- split by one per item',
                'value' => 'split_by_item'
            )
        );
    }

    /**
     * @param $orderItem
     * @return array
     */
    protected function execSaveEngravedToOrderNotesProcessOrderItem($orderItem)
    {
        $skipFlag = true;
        $result = array(
            'options' => array(),
            'message_exists_flag' => false
        );
        if (is_a($orderItem, 'Varien_Object')) {
            $skipFlag = false;
            $result['product_sku'] = @$orderItem->getSku();
            $result['product_name'] = @$orderItem->getName();
            $result['product_qty'] = @$orderItem->getQtyOrdered();
            try {
                $productOptionsAll = $orderItem->getProductOptions();
                if (!is_array($productOptionsAll) || empty($productOptionsAll)) {
                    return $result;
                }
                $productOptions = @$productOptionsAll['options'];
                if (is_array($productOptions) && !empty($productOptions)) {
                    foreach ($productOptions as $productOption) {
                        switch (@$productOption['option_type']) {
                            case 'field':
                            case 'area':
                                $result['options'][] = array(
                                    'label' => @$productOption['label'],
                                    'value' => @$productOption['value']
                                );
                                break;
                            case 'drop_down':
                            case 'radio':
                            case 'checkbox':
                            case 'multiple':
                            case 'date':
                            case 'date_time':
                            case 'time':
                                $result['options'][] = array(
                                    'label' => @$productOption['label'],
                                    'value' => @$productOption['print_value']
                                );
                                break;
                            default:
                                $result['options'][] = array(
                                    'label' => @$productOption['label'],
                                    'value' => '* Please, to see real value of this field - refer to this Order in Magento store *'
                                );
                                break;
                        }
                    }
                }
                if (count(@$result['options'])) {
                    $result['message_exists_flag'] = true;
                }
            } catch (\Exception $exception) {
                $result['message_exists_flag'] = false;
                $skipFlag = true;
            }
        }
        if (!$skipFlag) {
            $result['message_exists_flag'] = true;
        }
        return $result;
    }

    /**
     * @param array $registers registered parameters for current observer's check to be logged into CSV
     * @param array $eventData data corresponded to event
     */
    public function checkShippingRegions(&$registers, $eventData = array())
    {
        // 1. init configs
        $configurationRaw = static::checkShippingRegionsGetConfigs();
        $configuration = new Varien_Object($configurationRaw);
        // 2. check what is the exactly the event
        if ($configuration->getData('layout_handles_check_flag')) {
            $checkoutHandlesFlag = AWS_CSVLogger_Helper_Addons::checkHandle($configuration->getData('layout_handles'));
        } else {
            $checkoutHandlesFlag = true;
        }
        $checkoutControllersFlag = AWS_CSVLogger_Helper_Addons::checkController($configuration->getData('controllers_check'));
        $proceed = $checkoutHandlesFlag && $checkoutControllersFlag;
        if (!$proceed) {
            return ;
        }
        $step = static::checkShippingRegionsDetermineStep();
        // 3. perform action
        switch ($step) {
            case 'billing_save':
                $billingAddress = static::checkShippingRegionDetectSource('POST', 'billing');
                $sameAsBilling = $billingAddress->getData('use_for_shipping');
                if ($sameAsBilling) {
                    $allowed = static::checkShippingRegionsCheckAddress($billingAddress, $configuration);
                    if (!$allowed) {
                        AWS_CSVLogger_Helper_Data::setRequestParam('billing/use_for_shipping', 0);
                    }
                }
                break;
            case 'shipping_save':
                $shippingAddress = static::checkShippingRegionDetectSource('POST', 'shipping');
                $allowed = static::checkShippingRegionsCheckAddress($shippingAddress, $configuration);
                if (!$allowed) {
                    static::checkShippingRegionsForceRedirectToShippingStep($configuration);
                }
                break;
            case 'append_frontend_js':
                $requireJs = Mage::app()->getLayout()->getBlock($configuration->getData('frontend_requirejs_block_reference'));
                if ($requireJs) {
                    /** @var Mtaube_Requirejs_Block_Scripts $requireJs */
                    $requireJs->addModule($configuration->getData('frontend_requirejs_js_module'));
                    $blockContent = static::checkShippingRegionsRenderSettingsBlockForJs($configuration);
                    AWS_CSVLogger_Block_LatestStatic::addStaticContent(
                        $blockContent,
                        'bottom',
                        AWS_CSVLogger_Block_LatestStatic::STATIC_JS
                    );
                }
                break;
            case 'multishipping_save':
                $shippingAddresses = static::checkShippingRegionCollectForMultishipping('POST');
                $validationResult = static::checkShippingRegionProcessMultishippingAddresses($shippingAddresses, $configuration);
                if ($validationResult && $validationResult->getData('restricted_region')) {
                    static::checkShippingRegionsForceRedirectToShippingStepMultishipping($validationResult, $configuration);
                }
                break;
        }
    }

    /**
     * Specially for multishipping
     *
     * @param string $priority
     * @return array with addresses
     */
    public static function checkShippingRegionCollectForMultishipping($priority = 'POST')
    {
        $arrayFieldShip = 'ship';
        $arrayFieldAddressId = 'address';
        $arrayFieldQty = 'qty';
        $addresses = array();
        $params = Mage::app()->getRequest()->getParams();
        switch ($priority) {
            case null:
                // empty step
            case 'POST':
                if (is_array($params) && isset($params[$arrayFieldShip]) && is_array($params[$arrayFieldShip])) {
                    foreach ($params[$arrayFieldShip] as $shippingIndex => $shippingAddress) {
                        // collect only for items with weight/shippable
                        if (is_array($shippingAddress)) {
                            try {
                                $itemQuoteId = @key($shippingAddress);
                                $shippingAddress = @$shippingAddress[$itemQuoteId];
                                $addressId = @$shippingAddress[$arrayFieldAddressId];
                                $qty = @$shippingAddress[$arrayFieldQty];
                                $quoteItem = Mage::getModel('sales/quote_item');
                                @$quoteItem->load($itemQuoteId);
                                $addressRaw = array(
                                    'address_id' => $addressId
                                );
                                $productName = is_object($quoteItem) && $quoteItem->getId() ? $quoteItem->getName() : 'N/A';
                                $addressData = new Varien_Object($addressRaw);
                                $address = static::checkShippingRegionLoadFromSource($addressData);
                                if ($address) {
                                    $addresses[] = array(
                                        'address' => $address,
                                        'product_name' => $productName,
                                        'index' => $shippingIndex,
                                        'qty' => $qty
                                    );
                                }
                                // no unset here for $address/Data
                            } catch (Exception $exception) {
                            }
                        }
                    }
                }
                if (!is_null($priority) || count($addresses)) {
                    break;
                }
        }
        return $addresses;
    }

    /**
     * Validate array of addresses
     *
     * @param array $addresses
     * @param Varien_Object $configuration
     * @return Varien_Object
     */
    protected static function checkShippingRegionProcessMultishippingAddresses($addresses, $configuration)
    {
        $resultRaw = array(
            'restricted_region' => false,
            'regions' => ''
        );
        if (!is_array($addresses)) {
            return new Varien_Object($resultRaw);
        }
        foreach ($addresses as $addressIndex => $addressData) {
            if (is_array($addressData)) {
                $address = @$addressData['address'];
                $allowed = static::checkShippingRegionsCheckAddress($address, $configuration);
                if (!$allowed) {
                    $resultRaw['restricted_region'] = true;
                    $region = static::checkShippingRegionsGetRegionString($address);
                    $subResult = $addressData;
                    $subResult['region'] = $region;
                    $resultRaw['regions'][] = $subResult;
                }
            }
        }
        return new Varien_Object($resultRaw);
    }

    public static function checkShippingRegionsGetMultishippingMessage($validationResult, $configuration)
    {
        $cssClass = 'aws-csvlogger-shipping-restriction-warning-message';
        $cssStyle = $configuration->getData('multishipping_css_style');
        $mask = $configuration->getData('multishipping_mask');
        $message = $configuration->getData('multishipping_message');
        $items = array();
        $itemsRendered = '';
        $result = '';
        if ($validationResult && is_array($validationResult->getData('regions'))) {
            $regions = $validationResult->getData('regions');
            foreach ($regions as $index => $regionData) {
                if (!is_array($regionData)) {
                    continue;
                }
                $regionData['shipping_position'] = $index + 1;
                $itemRendered = AWS_CSVLogger_Helper_Data::replaceStringByMask($mask, $regionData);
                if (is_string($itemRendered) && strlen($itemRendered)) {
                    $items[] = $itemRendered;
                }
            }
        }
        if (is_array($items)) {
            $itemsRendered = implode("\r\n", $items);
            $result = AWS_CSVLogger_Helper_Data::replaceStringByMask($message, array('content' => $itemsRendered));
        }
        $cmsBlockMessageWarning = $configuration->getData('frontend_block_cms_block_content');
        if (is_string($cmsBlockMessageWarning) && strlen($cmsBlockMessageWarning)) {
            $result = $cmsBlockMessageWarning . '<br/>' . $result;
        }
        $result = '<div class="' . $cssClass . '" style="' . $cssStyle . '">' . $result . '</div>';
        return $result;
    }

    public static function checkShippingRegionsForceRedirectToShippingStepMultishipping($validationResult, $configuration)
    {
        $finalMessage = static::checkShippingRegionsGetMultishippingMessage($validationResult, $configuration);
        if (is_string($finalMessage) && strlen($finalMessage)) {
            Mage::getSingleton('core/session')->addWarning($finalMessage);
            // block controller from being processed to next multishipping step
            AWS_CSVLogger_Helper_Data::setRequestParam('continue', 0);
        }
        return true;
    }

    /**
     * Render block with JS variable contained restrictions' rules data
     *
     * @param Varien_Object $configuration
     * @return string rendered HTML
     */
    public static function checkShippingRegionsRenderSettingsBlockForJs($configuration)
    {
        $variableName = 'awsCsvLoggerAddonShippingRestrictionsRules';
        $contentMask = <<<HTML
try {
    window.{{variable_name}} = {{settings}};
} catch (e) {
    console.log('Error occurred while parsing settings for Shipping Restrictions: ' + e.message);
}
HTML;
        $settingsRaw = array(
            'allowed' => $configuration->getData('allowed'),
            'restricted' => $configuration->getData('restricted'),
            'allowStrategy' => $configuration->getData('allow_strategy'),
            'targetSelectorShow' => $configuration->getData('frontend_block_selector_append'),
            'targetSelectorListen' => $configuration->getData('frontend_region_selector'),
            'cmsWarningContent' => $configuration->getData('frontend_block_cms_block_content'),
            'defaultCssStyles' => $configuration->getData('frontend_block_cms_css'),
            'continueButtons' => $configuration->getData('frontend_continue_buttons'),
        );
        $settings = Mage::helper('core')->jsonEncode($settingsRaw, true);
        $content = AWS_CSVLogger_Helper_Data::replaceStringByMask(
            $contentMask,
            array(
                'variable_name' => $variableName,
                'settings' => $settings
            )
        );
        $output = AWS_CSVLogger_Helper_Data::renderCustomContent($content);
        return $output;
    }

    /**
     * Return back to shipping method
     *
     * @param Varien_Object $configuration configurations for this addon
     * @return bool true if ok
     */
    public static function checkShippingRegionsForceRedirectToShippingStep($configuration)
    {
        $message = $configuration->getData('shipping_step_error_message');
        $result = array(
            'error' => 1,
            'message' => $message,
            'goto_section' => 'shipping'
        );
        // not used as there is not ajax update for this step
        if (false) {
            $layoutHandle = $configuration->getData('shipping_render_layout_handle');
            $result['update_section'] = array(
                'name' => 'shipping',
                'html' => ''
            );
            $layout = Mage::app()->getLayout();
            $update = $layout->getUpdate();
            $update->load($layoutHandle);
            $layout->generateXml();
            $layout->generateBlocks();
            $output = $layout->getOutput();
            $result['update_section']['html'] = $output;
        }
        Mage::app()->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($result)
        );
        // for disabling native controller from being executed
        $_SERVER['REQUEST_METHOD'] = 'EX-POST';
        return true;
    }

    /**
     * Determin current step of shipping restriction processing
     *
     * @return string which action to perform
     */
    public static function checkShippingRegionsDetermineStep()
    {
        $isBillingHandle = AWS_CSVLogger_Helper_Addons::checkController(array('/checkout_[^_]+_saveBilling/'));
        if ($isBillingHandle) {
            return 'billing_save';
        }
        $isShippingHandle = AWS_CSVLogger_Helper_Addons::checkController(array('/checkout_[^_]+_saveShipping/'));
        if ($isShippingHandle) {
            return 'shipping_save';
        }
        $isIndexHandle = AWS_CSVLogger_Helper_Addons::checkController(array('/checkout_[^_]+_index/'));
        if ($isIndexHandle) {
            return 'append_frontend_js';
        }
        $isMultishippingAddressesHandle = AWS_CSVLogger_Helper_Addons::checkController(array('checkout_multishipping_addressesPost'));
        if ($isMultishippingAddressesHandle) {
            return 'multishipping_save';
        }
        return '';
    }

    /**
     * Get address
     *
     * @param string|null $priority from which source get data about address
     * @param string $addressType
     * @return bool|Mage_Customer_Model_Address
     */
    protected static function checkShippingRegionDetectSource($priority = null, $addressType = 'shipping')
    {
        $address = Mage::getModel('customer/address');
        $addressData = null;
        $params = Mage::app()->getRequest()->getParams();
        switch ($priority) {
            case null:
                // empty step
            case 'POST':
                if (is_array($params)) {
                    $proceedToParams = true;
                    $field = isset($params[$addressType]) ? $addressType : '';
                    $addressBookKey = $field . '_address_id';
                    if (isset($params[$addressBookKey]) && strlen($params[$addressBookKey])) {
                        $id = $params[$addressBookKey];
                        $addressData = new Varien_Object(array('address_id' => $id));
                        $address = static::checkShippingRegionLoadFromSource($addressData);
                        if ($address && isset($params[$field]['use_for_shipping'])) {
                            $address->setData('use_for_shipping', $params[$field]['use_for_shipping']);
                            $proceedToParams = !is_object($address);
                        }
                    } if (strlen($field) && is_array($params[$field]) && $proceedToParams) {
                        $addressData = new Varien_Object($params[$field]);
                        $address = static::checkShippingRegionLoadFromSource($addressData);
                    }
                }
                if (!is_null($priority) || $address) {
                    break;
                }
            case 'checkout':
                $checkoutSession = Mage::getSingleton('checkout/session');
                $quote = $checkoutSession->getQuote();
                switch ($addressType) {
                    case 'shipping':
                        $buffer = $quote->getShippingAddress();
                        break;
                    case 'billing':
                        $buffer = $quote->getBillingAddress();
                        break;
                }
                $address = static::checkShippingRegionLoadFromSource($buffer->getData());
        }
        return $address;
    }

    /**
     * Try to load address from Varien_Object
     *
     * @param Varien_Object $object
     * @return bool|Mage_Customer_Model_Address
     */
    protected static function checkShippingRegionLoadFromSource($object)
    {
        $address = Mage::getModel('customer/address');
        // 1. try recreate address data
        $address->addData($object->getData());
        if (
            ($address->hasData('country_id')) &&
            ($address->hasData('region_code') || $address->hasData('region_id') || $address->hasData('region'))
        ) {
            @$address->unsetData($address->getIdFieldName());
            @$address->unsetData('address_id');
            return $address;
        }
        // 2. try to determine it via ID
        if ($object->hasData('address_id')) {
            $buffer = Mage::getModel('customer/address');
            $buffer->load($object->getData('address_id'));
            if ($buffer->getId()) {
                @$address->setData(array())->addData($buffer->getData())->unsetData($buffer->getIdFieldName())->unsetData('address_id');
                return $address;
            }
        }
        return false;
    }

    /**
     * @return array configs for AWS_CSVLogger_Helper_AddonsRepo::checkShippingRegions
     */
    protected static function checkShippingRegionsGetConfigs()
    {
        $requireJsReference = 'requirejs';
        $moduleName = 'checkout/shipping-restriction';
        $allowedControllers = array('/checkout_[^_]+_index/', '/checkout_[^_]+_saveBilling/', '/checkout_[^_]+_saveShipping/', '/checkout_multishipping_[^_]/');
        $allowedHandles = array('checkout_onepage_index', 'checkout_osc_index', 'checkout_onestep_index');
        $handlesCheckFlag = false;
        $allowedRegions = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/allowed_regions');
        if (!is_string($allowedRegions) || !strlen($allowedRegions)) {
            $allowedRegions = '';
        }
        $allowedRegions = AWS_CSVLogger_Helper_Data::getArrayFromTextAreaStatic($allowedRegions);
        $restrictedRegions = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/restricted_regions');
        if (!is_string($restrictedRegions) || !strlen($restrictedRegions)) {
            $restrictedRegions = '';
        }
        $restrictedRegions = AWS_CSVLogger_Helper_Data::getArrayFromTextAreaStatic($restrictedRegions);
        $popupMessageWrongRegion = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/popup_message_wrong_region');
        if (!is_string($popupMessageWrongRegion) || !strlen($popupMessageWrongRegion)) {
            $popupMessageWrongRegion = 'Restricted shipping region.';
        }
        $allowStrategyEnabled = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/allow_strategy');
        if (!is_string($allowStrategyEnabled) && !is_bool($allowStrategyEnabled)) {
            $allowStrategyEnabled = true;
        } else {
            $allowStrategyEnabled = (bool)$allowStrategyEnabled;
        }
        $cmsBlockMessageWarning = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/cms_block_message_warning');
        if (!is_string($cmsBlockMessageWarning) && !is_int($cmsBlockMessageWarning)) {
            $cmsBlockMessageWarning = '';
        } else {
            $cmsBlockMessageWarning = @AWS_CSVLogger_Helper_Data::renderBlockStatic($cmsBlockMessageWarning);
        }
        $selectorForAppend = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/css_selector');
        if (!is_string($selectorForAppend) || !strlen($selectorForAppend)) {
            $selectorForAppend = false;
        }
        $selectorForListen = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/css_selector_listen');
        if (!is_string($selectorForListen) || !strlen($selectorForListen)) {
            $selectorForAppend = false;
        }
        $cmsBlockCssStyles = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/show_block_styles');
        if (!is_string($cmsBlockCssStyles) || !strlen($cmsBlockCssStyles)) {
            $cmsBlockCssStyles = false;
        }
        $continueButtonsSelector = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/continue_buttons_selector');
        if (!is_string($continueButtonsSelector) || !strlen($continueButtonsSelector)) {
            $continueButtonsSelector = false;
        }
        $multishippingWarningMessage = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/multishipping_warning_message');
        if (is_null($multishippingWarningMessage) && !is_string($multishippingWarningMessage) || !strlen($multishippingWarningMessage)) {
            $multishippingWarningMessage = 'Restricted shipping region specified for some next item(s): <ol>{{content}}</ol>';
        }
        $multishippingWarningMask = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/multishipping_warning_mask');
        if (!is_string($multishippingWarningMask) || !strlen($multishippingWarningMask)) {
            $multishippingWarningMask = '<li>{{product_name}} x {{qty}}: region {{region}}</li>';
        }
        $multishippingCssStyle = Mage::getStoreConfig('aws_csvlogger/shipping_restriction_regions/multishipping_css_block_style');
        if (!is_string($multishippingCssStyle) || !strlen($multishippingCssStyle)) {
            $multishippingCssStyle = '';
        } else {
            $multishippingCssStyle = AWS_CSVLogger_Helper_Data::escapeCssText($multishippingCssStyle);
        }
        return array(
            'allowed' => $allowedRegions,
            'restricted' => $restrictedRegions,
            'allow_strategy' => $allowStrategyEnabled,
            'layout_handles' => $allowedHandles,
            'layout_handles_check_flag' => $handlesCheckFlag,
            'controllers_check' => $allowedControllers,
            'shipping_render_layout_handle' => 'checkout_onepage_shipping', // not used
            'shipping_step_error_message' => $popupMessageWrongRegion,
            'frontend_requirejs_block_reference' => $requireJsReference,
            'frontend_requirejs_js_module' => $moduleName,
            'frontend_block_selector_append' => $selectorForAppend,
            'frontend_block_cms_block_content' => $cmsBlockMessageWarning,
            'frontend_region_selector' => $selectorForListen,
            'frontend_block_cms_css' => $cmsBlockCssStyles,
            'frontend_continue_buttons' => $continueButtonsSelector,
            'multishipping_message' => $multishippingWarningMessage,
            'multishipping_mask' => $multishippingWarningMask,
            'multishipping_css_style' => $multishippingCssStyle
        );
    }

    /**
     * @param Mage_Sales_Model_Quote_Address $address current address (billing or shipping)
     * @param Varien_Object $configuration object with configs
     * @return bool true if region is allowed
     */
    protected static function checkShippingRegionsCheckAddress($address, $configuration)
    {
        $regionString = static::checkShippingRegionsGetRegionString($address);
        if (!is_string($regionString)) {
            return true;
        }
        $allowedRegions = $configuration->getData('allowed');
        $restrictedRegions = $configuration->getData('restricted');
        $allowedStrategyFlag = $configuration->getData('allow_strategy');
        $allowedFlag = (bool)in_array($regionString, $allowedRegions);
        $restrictedFlag = (bool)in_array($regionString, $restrictedRegions);
        $accept = true;
        switch ($allowedStrategyFlag) {
            case true:
                $accept = $allowedFlag || (!$allowedFlag && !$restrictedFlag);
                break;
            case false:
                $accept = $allowedFlag;
                break;
        }
        return $accept;
    }

    public static function checkShippingRegionsGetRegionString($object)
    {
        if (!is_a($object, 'Varien_Object')) {
            return true;
        }
        // 1. check region string
        $regionString = $object->getRegion();
        if (is_string($regionString) && strlen($regionString)) {
            return $regionString;
        }
        // 2. check region by ID
        $regionId = $object->getRegionId();
        if ($regionId) {
            /** @var Mage_Directory_Model_Region $region */
            $region = Mage::getModel('directory/region')->load($regionId);
            if ($region->getId()) {
                $regionString = $region->getName();
                if (is_string($regionString) && strlen($regionString)) {
                    return $regionString;
                }
            }
        }
        // 3. check by code
        $regionCode = $object->getRegionCode();
        if ($regionId) {
            /** @var Mage_Directory_Model_Region $region */
            $region = Mage::getModel('directory/region')->loadByCode($regionCode);
            if ($region->getId()) {
                $regionString = $region->getName();
                if (is_string($regionString) && strlen($regionString)) {
                    return $regionString;
                }
            }
        }
        return false;
    }

    /**
     * Dynamically inject top/bottom blocks with inserted js/css content
     *
     * @param $registers
     * @param array $eventData
     */
    public function renderLatestStatic(&$registers, $eventData = array())
    {
        $configRaw = static::renderLatestStaticGetConfig();
        $config = new Varien_Object($configRaw);
        static::renderLatestStaticInitStaticPull($config->getData('rules'));
        try {
            $layout = Mage::getSingleton('core/layout');
            static::renderLatestStaticAppendBlock($layout, $config, 'top', 'after_body_start');
            static::renderLatestStaticAppendBlock($layout, $config, 'bottom', 'before_body_end');
        } catch (Exception $exception) {}
    }

    /**
     * Create and append LatestStatic blocks into layout
     *
     * @param Mage_Core_Model_Layout $layout
     * @param Varien_Object $config
     * @param string $key `top` or `bottom`
     * @param string $parentBlockName block's name to append to
     */
    protected static function renderLatestStaticAppendBlock($layout, $config, $key = 'top', $parentBlockName = 'content')
    {
        try {
            $blockName = sprintf($config->getData('block_name_mask'), $key);
            /** @var   */
            $block = $layout->createBlock(
                'AWS_CSVLogger_Block_LatestStatic',
                $blockName,
                array('template' => $config->getData('template_path_' . $key))
            );
            $blockParent = $layout->getBlock($parentBlockName);
            if ($blockParent) {
                $blockParent->append($block);
            }
        } catch (Exception $exception) {}
    }

    /**
     * Get basic configs for LatestStatic block
     *
     * @return array basic configurations for LatestStatic
     */
    public static function renderLatestStaticGetConfig()
    {
        $rulesSet = Mage::getStoreConfig('aws_csvlogger/latest_static/manage_set');
        if (!is_string($rulesSet) || !strlen($rulesSet)) {
            $rulesSet = '';
        }
        /** @var Aws_CSVLogger_Helper_Data $helper */
        $helper = Mage::helper('aws_csvlogger');
        $rules = $helper->generateWritableColumns($rulesSet, false);

        return array(
            'template_path_bottom' => 'aws/common/latest_static_bottom.phtml',
            'template_path_top' => 'aws/common/latest_static_top.phtml',
            'block_name_mask' => 'aws.common.latest-static.%s',
            'rules' => $rules
        );
    }

    /**
     * Append static content files into buffer
     *
     * @param array $rules preset of where and when to append static files via latest block
     */
    public static function renderLatestStaticInitStaticPull($rules)
    {
        if (is_array($rules)) {
            /**
             * @var string $genericGroup generic group
             * @var array $groupData file path, controllers applied to, top/bottom
             */
            foreach ($rules as $genericGroup => $groupData) {
                foreach ($groupData as $filePath => $fileRules) {
                    foreach ($fileRules as $controller => $appendPlace) {
                        if (AWS_CSVLogger_Helper_Addons::checkController($controller)) {
                            $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
                            AWS_CSVLogger_Block_LatestStatic::addStatic($filePath, $appendPlace, $ext);
                        }
                    }
                }
            }
        }
    }
}