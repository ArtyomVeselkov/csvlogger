<?php

$installer = $this;
$installer->startSetup();

if (is_null(Mage::helper('aws_csvlogger')->getStoreConfigValueDirect(AWS_CSVLogger_Helper_Data::MAGE_CORE_CONFIG_SALT_FOR_HASH_INTER_CTRL_KEY))) {
    Mage::getConfig()->saveConfig(
        AWS_CSVLogger_Helper_Data::MAGE_CORE_CONFIG_SALT_FOR_HASH_INTER_CTRL_KEY,
        Mage::helper('core')->getRandomString(32)
    );
}

if (is_null(Mage::helper('aws_csvlogger')->getStoreConfigValueDirect('aws_csvlogger/unilogin_options/unilogin_password_prefix'))) {
    Mage::getConfig()->saveConfig(
        'aws_csvlogger/unilogin_options/unilogin_password_prefix',
        Mage::helper('core')->getRandomString(5)
    );
}

$table = $installer->getConnection()
    ->newTable($installer->getTable('aws_csvlogger/addonscalls'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Call Id')
    ->addColumn('hash', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Security Hash')
    ->addColumn('esi_md5', Varien_Db_Ddl_Table::TYPE_TEXT, 127, array(
        'nullable'  => true
    ), 'ESI Hash')
    ->addColumn('user_caller', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true
    ), 'User Identifiers')
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true
    ), 'Message Text')
    ->addColumn('call_times', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false
    ), 'Times of Call')
    ->addColumn('enabled', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => true
    ), 'Active Call')
    ->addColumn('params', Varien_Db_Ddl_Table::TYPE_BLOB, 2056, array(
        'nullable'  => true
    ), 'Params')
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => true,
    ), 'Creation Time')
    ->addIndex($installer->getIdxName('aws_csvlogger/addonscalls', array('hash')),
        array('hash'))
    ->setComment('CSV Logger AddonsCalls Stack');
$installer->getConnection()->createTable($table);

$installer->endSetup();