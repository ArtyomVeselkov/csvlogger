<?php

class AWS_CSVLogger_Block_Adminhtml_View extends Mage_Adminhtml_Block_Template
{
    public function getLogFiles()
    {
        $logFiles = array();
        $logFolderPath = Mage::getBaseDir('var') . DS . 'log';

        if (!file_exists($logFolderPath)) {
            mkdir($logFolderPath, 0755, true);
        }

        $directory = new DirectoryIterator($logFolderPath);

        foreach ($directory as $fileInfo) {
            if (!$fileInfo->isFile() || !preg_match('/\.(?:log)$/', $fileInfo->getFilename())) {
                continue;
            }

            $logFiles[] = $fileInfo->getFilename();
        }

        return $logFiles;
    }
    public function getDefaultLogsSelectorPrefix()
    {
        return 'logsGridContainer';
    }

    public function getDefaultLogviewerPrefix()
    {
        return 'gridContainer';
    }

    public function getProfiles()
    {
        return '[0]';
    }

    public function getDefaultProfile()
    {
        return '0';
    }
}
