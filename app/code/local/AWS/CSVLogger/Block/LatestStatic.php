<?php

class AWS_CSVLogger_Block_LatestStatic extends Mage_Core_Block_Template
{
    const STATIC_JS = 'js';
    const STATIC_CSS = 'css';
    const DEFAULT_RENDER_IF_NONE = '<!-- Render here {{{path}}}, but no file were found or empty -->';
    const DEFAULT_CSS_WRAPPER = "\r\n<style type=\"text/css\">/* PATH: {{path}} */\r\n{{content}}</style>\r\n";
    const DEFAULT_JS_WRAPPER = "\r\n<script type=\"text/javascript\">/* PATH: {{path}} */\r\n{{content}}</script>\r\n";
    protected static $_renderCssArray = array();
    protected static $_renderJsArray = array();

    public function addCss($pathToStatic, $renderAt = 'bottom', $staticType = self::STATIC_CSS)
    {
        return static::addCss($pathToStatic, $renderAt, self::STATIC_CSS);
    }

    public static function addStaticContent($content, $renderAt = 'bottom', $staticType = self::STATIC_CSS, $note = 'static')
    {
        if (!is_string($content) || !strlen($content)) {
            $buffer = AWS_CSVLogger_Helper_Data::replaceStringByMask(self::DEFAULT_RENDER_IF_NONE, array('path' => $note));
        } else {
            try {
                switch ($staticType) {
                    case self::STATIC_CSS:
                        $buffer = AWS_CSVLogger_Helper_Data::replaceStringByMask(
                            self::DEFAULT_CSS_WRAPPER,
                            array('path' => $note, 'content' => $content)
                        );
                        break;
                    case self::STATIC_JS:
                        $buffer = AWS_CSVLogger_Helper_Data::replaceStringByMask(
                            self::DEFAULT_JS_WRAPPER,
                            array('path' => $note, 'content' => $content)
                        );
                        break;
                    default:
                        $buffer = '';
                }
                if (is_string($buffer) && strlen($buffer)) {
                    switch ($staticType) {
                        case self::STATIC_JS:
                            static::$_renderJsArray[$renderAt][] = $buffer;
                            break;
                        case self::STATIC_CSS:
                            static::$_renderCssArray[$renderAt][] = $buffer;
                            break;
                    }
                }
            } catch (\Exception $exception) {
                ;
            }
        }
    }

    public static function addStatic($pathToStatic, $renderAt = 'bottom', $staticType = self::STATIC_CSS)
    {
        $realPathToCss = AWS_CSVLogger_Helper_Data::getRealPathToSkinFile($pathToStatic);
        $content = '';
        if (@file_exists($realPathToCss)) {
            $content = @file_get_contents($realPathToCss);
        }
        static::addStaticContent($content, $renderAt, $staticType, $pathToStatic);
    }

    public function renderPack($renderAt = null)
    {
        return static::renderPackStatic($renderAt);
    }

    public static function renderPackStatic($renderAt = null)
    {
        $renderPacks = array();
        $resultArray = array();
        if (!is_string($renderAt) || !strlen($renderAt)) {
            return '';
        } elseif (is_null($renderAt)) {
            $renderPacks = array_keys(static::$_renderCssArray);
        } elseif (is_string($renderAt)) {
            $renderPacks = array($renderAt);
        } elseif (is_array($renderAt)) {
            $renderPacks = $renderAt;
        }
        foreach ($renderPacks as $renderPack) {
            if (is_string($renderPack) && isset(static::$_renderCssArray[$renderPack])) {
                $resultArray = array_merge($resultArray, static::$_renderCssArray[$renderPack]);
            }
            if (is_string($renderPack) && isset(static::$_renderJsArray[$renderPack])) {
                $resultArray = array_merge($resultArray, static::$_renderJsArray[$renderPack]);
            }
        }
        $result = @implode("\r\n", $resultArray);
        return $result;
    }
}