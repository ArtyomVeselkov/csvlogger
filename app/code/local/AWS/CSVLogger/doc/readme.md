[Miami] AWS CSVLogger
=====================
*(this is a draft of extension's description)*

Main Functions
---------------
**Intro**

Main purpose of this extension: make Magento's store supporting more easy.
The fist main feature is logging every selected data of both customers and administrators (by default logging of all administrator's activity is disabled). \\
The second one is comfortable for working admin's grid for analyzing logged data. \\
The third one -- extensibility be addons, which could retirve more data from each store's visit and  try to fix some common problems (for example, typos in coupons' codes). \\

**Main features**
1. Log main data about every visitor of the store. You can choose what data to record and how to show it in the final grid. 

2. No code overwriting: all data is recorded by observers, which (except addons for this extension) don't change any live data.

3. Flexible and light final report `AWS CSV Logger Live View`

   - Ajax loading;
   
   - By day splitting with ability to load any sequence of days' logs;
   
   - Flexible filters;
   
   - Ability to show sub-report on selected log's items.
   
4. Addons which extend the functionality of extension:

   - Log POST parameters (for site supporting)
   
   - Validate and fix coupons code with typos
   
   - Login as Customer either from Admin Panel or from FrontEnd. \\
     Login as Customer from Admin Panel has a three level security checking, which makes this functionality safe. \\
     No one can access another's account except single purpose administrator's access within Admin Panel.
     
   - Log payment error with specifying reason from merchant. \\
     Then You can analyze this data for each customer in chronological way, step by step.
   
F.A.Q.
---------------
* There is no one log file to select in the first grid.
  1. If You have just installed extension, there could be no data to be logged yet.
     Try go to hte FrontEnd and refresh page with logger.
  2. Check either there are needed permissions for the destination directory for keeping log files.
  3. Check, if the destination directory exists. By default it is `var/log/logger`
* I can't see button _Login As Customer_ on the Customer's page.
  1. Check, if You have enabled `Enable addons' controller` in the 
     `Admin Panel > System > Configuration > AWS CSV Logger > Addons Options`
  2. Check, if You have selected corresponded addon in the multiselect box `Select Addons`
  3. Check, if You have mask's url for admin route in the `Url to exclude`.
     If You have -- check if exclude part is present for event `adminhtml_widget_container_html_before` like below:
     ```
      # exclude all adminpanel observations
     ^\/admin\/.* | adminhtml_widget_container_html_before
     ```