<?php

class AWS_CSVLogger_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->_redirect('/');
    }

    public function addonAction()
    {
        $addonHelper = Mage::helper('aws_csvlogger/addons');
        $params = $addonHelper->unholdSecurityData(true);
        if (!$params || !is_array($params) || 0 == count($params)) {
            return $this->_redirect('/');
        }
        $addonAction = isset($params['addon_action']) ? $params['addon_action'] : null;
        $result = false;

        if (!$addonAction || 3 > strlen($addonAction) || (!isset($params['from_frontend']))) {
            return $this->_redirect('/');
        }

        $helperAddonsRepo = Mage::helper('aws_csvlogger/addonsRepo');
        if (method_exists($helperAddonsRepo, $addonAction)) {
            try {
                $result = $helperAddonsRepo->{$addonAction}($params);
            } catch (Exception $e) {
                ;
            }
        }

        if ($result && isset($result['url'])) {
            return $this->_redirect($result['url'], isset($result['params']) ? $result['params'] : array());
        } else {
            return $this->_redirect('/');
        }
    }
}