<?php

/**
 * Class AWS_CSVLogger_Adminhtml_LoggerController
 *
 */

/**
 * TODO Live View of log files splitted by date, code, trace etc
 */
class AWS_CSVLogger_Adminhtml_LoggerController extends Mage_Adminhtml_Controller_Action
{
    /**
     * structure
     * <action>_<profile>_<direction>_<param>
     */
    const PARAM_DELIM = '_';
    const PARAM_ACTIONS = 'actions';
    const PARAM_PROFILE = 'profile';
    const PARAM_ACTION_LOADFILE = 'loadFile';
    const PARAM_ACTION_LOADCOLUMNSLAYOUT = 'loadColumnsLayout';
    const PARAM_ACTION_SERVICE_ARRAY = 'servdesc';
    const PARAM_ACTION_GETFILES = 'getFiles';
    const PARAM_PREFIX_DIRECTION_IN = 'in';
    const PARAM_PREFIX_DIRECTION_OUT = 'out';
    const PARAM_ACTION_LOADFILE_POSTFIX_POSITION = 'position';
    const PARAM_ACTION_LOADFILE_POSTFIX_FILETYPE = 'fileType';
    const PARAM_ACTION_LOADFILE_POSTFIX_FILENAME = 'fileName';
    const PARAM_ACTION_LOADFILE_POSTFIX_REFRESH = 'refresh';
    const PARAM_ACTION_LOADFILE_POSTFIX_MD5 = 'md5';
    const PARAM_ACTION_LOADFILE_POSTFIX_DATA = 'data';
    const PARAM_ACTION_LOADCOLUMNSLAYOUT_POSTFIX_DATA = 'data';
    const PARAM_ACTION_LOADFILE_COUNT = 'count';
    const PARAM_ACTION_GETFILES_POSTFIX_LIST = 'list';
    const PARAM_ACTION_LOADFILE_OUT_STATUS = 'status';
    const PARAM_ACTION_LOADFILE_OUT_STATUS_REFRESHED = 'refreshed';
    const PARAM_ACTION_LOADFILE_OUT_STATUS_UPDATED = 'updated';
    const PARAM_ACTION_LOADFILE_OUT_STATUS_SAMEORERROR = 'sameOrError';

    protected $_params = array(
        self::PARAM_ACTIONS => array(
            self::PARAM_PROFILE,
            self::PARAM_ACTION_LOADFILE => array(
                'indexed' => array(
                    self::PARAM_ACTION_LOADFILE_POSTFIX_POSITION,
                    self::PARAM_ACTION_LOADFILE_POSTFIX_FILETYPE,
                    self::PARAM_ACTION_LOADFILE_POSTFIX_FILENAME,
                    self::PARAM_ACTION_LOADFILE_POSTFIX_REFRESH,
                    self::PARAM_ACTION_LOADFILE_POSTFIX_MD5
                ),
                self::PARAM_PREFIX_DIRECTION_IN => array(
                    self::PARAM_ACTION_LOADFILE_COUNT
                ),
                self::PARAM_PREFIX_DIRECTION_OUT => array(
                    self::PARAM_ACTION_LOADFILE_OUT_STATUS => array(
                        self::PARAM_ACTION_LOADFILE_OUT_STATUS_REFRESHED,
                        self::PARAM_ACTION_LOADFILE_OUT_STATUS_UPDATED,
                        self::PARAM_ACTION_LOADFILE_OUT_STATUS_SAMEORERROR
                    )
                )
            ),
            self::PARAM_ACTION_LOADCOLUMNSLAYOUT => array(
                self::PARAM_PREFIX_DIRECTION_OUT => array(
                    self::PARAM_ACTION_LOADCOLUMNSLAYOUT_POSTFIX_DATA
                )
            ),
            self::PARAM_ACTION_GETFILES => array(
                self::PARAM_PREFIX_DIRECTION_OUT => array(
                    self::PARAM_ACTION_GETFILES_POSTFIX_LIST
                )
            ),
            self::PARAM_ACTION_SERVICE_ARRAY
        ),
        'directions' => array(
            self::PARAM_PREFIX_DIRECTION_IN,
            self::PARAM_PREFIX_DIRECTION_OUT
        )
    );

    protected function _getLogFiles(&$out, $params)
    {
        $helper = Mage::helper('aws_csvlogger');
        $d = self::PARAM_DELIM;
        $xi = self::PARAM_ACTION_GETFILES . $d . self::PARAM_PREFIX_DIRECTION_OUT . $d . self::PARAM_ACTION_GETFILES_POSTFIX_LIST;
        $out[$xi] = $helper->getLogFiles();
    }

    protected static function _getParam($params, $param, $default = null, $type = 's')
    {
        $result = null;
        $result = isset($params[$param]) ? $params[$param] : $default;
        switch ($type) {
            case 'i':
                return @intval($result);
                break;
            case 'b':
                // PHP >= 5.5 needed
                // return @boolval($result);
                return (bool) $result;
                break;
            case 's':
            default:
                return $result;
        }
        return $result;
    }

    protected static function _loadFile(&$out, $params)
    {
        $d = self::PARAM_DELIM;
        $_xi = self::PARAM_ACTION_LOADFILE . $d . self::PARAM_PREFIX_DIRECTION_IN . $d;
        $_xo = self::PARAM_ACTION_LOADFILE . $d . self::PARAM_PREFIX_DIRECTION_OUT . $d;

        $helper = Mage::helper('aws_csvlogger');
        $filesCount = static::_getParam($params, $_xi . self::PARAM_ACTION_LOADFILE_COUNT, 1, 'i');

        $ourArr = array();
        for ($index = 1; $index <= $filesCount; $index++) {
            $xi = self::PARAM_ACTION_LOADFILE . $d . self::PARAM_PREFIX_DIRECTION_IN . $d . $index . $d;
            $xo = self::PARAM_ACTION_LOADFILE . $d . self::PARAM_PREFIX_DIRECTION_OUT . $d . $index . $d;

            $position = static::_getParam($params, $xi . self::PARAM_ACTION_LOADFILE_POSTFIX_POSITION, 0, 'i');
            $fileType = static::_getParam($params, $xi . self::PARAM_ACTION_LOADFILE_POSTFIX_FILETYPE, 'csv');
            $fileName = static::_getParam($params, $xi . self::PARAM_ACTION_LOADFILE_POSTFIX_FILENAME, '');

            // possible: partly_if_can, refresh
            $refresh = static::_getParam($params, $xi . self::PARAM_ACTION_LOADFILE_POSTFIX_REFRESH, true, 'b');
            $md5_remote = static::_getParam($params, $xi . self::PARAM_ACTION_LOADFILE_POSTFIX_MD5, '');
            $md5_current = $helper->getLogMd5($fileName);

            $ourArr[$fileName] = array();
            switch ($fileType) {
                case 'csv' :
                    if (($md5_current != $md5_remote) || ($refresh)) {
                        $ourArr[$fileName][self::PARAM_ACTION_LOADFILE_POSTFIX_DATA] = $helper->loadLogCSV($fileName, $refresh ? 0 : $position);
                        $ourArr[$fileName][self::PARAM_ACTION_LOADFILE_POSTFIX_MD5] = $md5_current;
                        $ourArr[$fileName][self::PARAM_ACTION_LOADFILE_POSTFIX_POSITION] = $helper->getLogSize($fileName) + 1; // BE AWARE HERE ! May use ftell
                        $ourArr[$fileName][self::PARAM_ACTION_LOADFILE_OUT_STATUS] = $refresh ? self::PARAM_ACTION_LOADFILE_OUT_STATUS_REFRESHED : self::PARAM_ACTION_LOADFILE_OUT_STATUS_UPDATED;
                    } else {
                        $ourArr[$fileName][self::PARAM_ACTION_LOADFILE_OUT_STATUS] = self::PARAM_ACTION_LOADFILE_OUT_STATUS_SAMEORERROR;
                    }
                    break;
            }
        }
        $out[$_xo . self::PARAM_ACTION_LOADFILE_POSTFIX_DATA] = $ourArr;
    }

    protected function _loadColumnsLayout(&$out, $params)
    {
        $helper = Mage::helper('aws_csvlogger');
        $d = self::PARAM_DELIM;
        $xi = self::PARAM_ACTION_LOADCOLUMNSLAYOUT . $d . self::PARAM_PREFIX_DIRECTION_OUT . $d . self::PARAM_ACTION_LOADCOLUMNSLAYOUT_POSTFIX_DATA;
        $out[$xi] = $helper->getColumnsLayout();
    }

    public function indexAction()
    {
        $this->_redirect('*/*/view');
    }

    public function viewAction()
    {
        if (null != $this->getRequest()->getParam('actions', null)) {
            $this->tailAction();
        } else {
            $this->loadLayout();
            $this->_setActiveMenu('system/aws_csvlogger/index_view');
            $this->renderLayout();
        }
    }

    public function tailAction()
    {
        $params = $this->getRequest()->getParams();
        $dataActions = $params['actions'];
        if (is_string($dataActions) && ('' != $dataActions)) {
            $out = array();
            $actions = explode(',', $dataActions);
            foreach ($actions as $action) {
                switch ($action) {
                    case 'loadFile':
                        $this->_loadFile($out, $params);
                        break;
                    case 'loadColumnsLayout':
                        $this->_loadColumnsLayout($out, $params);
                        break;
                    case 'getFiles':
                        $this->_getLogFiles($out, $params);
                        break;
                }
            }
            $outObj = (object) $out;
            $response = Zend_Json::encode($outObj);
            $this->getResponse()->setBody($response);
        }
    }

    public function addonAction()
    {
        $params = $this->getRequest()->getParams();
        unset($params['key']);
        $addonAction = isset($params['addon_action']) ? $params['addon_action'] : null;
        $addonHelper = Mage::helper('aws_csvlogger/addons');
        $result = false;

        if (!$addonAction || 3 > strlen($addonAction)) {
            return $this->_redirect('admin/');
        }

        if (isset($params['from_frontend'])) {
            if ($params = $addonHelper->holdSecurityData()) {
                return $this->_redirect('aws_csvlogger/index/addon', array('_query' => $params, '_nosecret' => '_nosecret'));
            } else {
                return $this->_redirect('*/');
            }
        }

        $params = $addonHelper->unholdSecurityData(true);
        $helperAddonsRepo = Mage::helper('aws_csvlogger/addonsRepo');
        if (method_exists($helperAddonsRepo, $addonAction)) {
            try {
                $result = $helperAddonsRepo->{$addonAction}($params);
            } catch (Exception $e) {
                ;
            }
        }

        if ($result && isset($result['url'])) {
            return $this->_redirect($result['url'], isset($result['patams']) ? $result['patams'] : array());
        } else {
            return $this->_redirect('admin/');
        }
    }

    function _isAllowed()
    {
        $flagEnabled = Mage::getStoreConfigFlag('aws_csvlogger/options/enable');
        $flagAcl = Mage::getSingleton('admin/session')->isAllowed('system/aws_csvlogger');

        return $flagEnabled && $flagAcl;
    }
}
