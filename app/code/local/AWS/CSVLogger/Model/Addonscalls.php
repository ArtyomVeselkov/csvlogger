<?php

class AWS_CSVLogger_Model_Addonscalls extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('aws_csvlogger/addonscalls', 'id');
    }

    public function _beforeSave()
    {
        $this->setCreationTime(time());
        return parent::_beforeSave();
    }
}