<?php

class AWS_CSVLogger_Model_Resource_Addonscalls_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    const DEFAULT_MAX_TIMES_CALL = 1;
    const DEFAULT_MAX_CALL_LIFETIME = 60;

    public function _construct()
    {
        parent::_construct();
        $this->_init('aws_csvlogger/addonscalls');
    }
    
    public function getActiveAddonsCallByHash($hash)
    {
        $this->addFieldToFilter('hash', $hash);
        $this->addFieldToFilter('enabled', '1');
        
        return $this;
    }

    public function getExpiredAddonsCalls($expDelta)
    {
        if (!is_numeric((string)$expDelta)) {
            $expDelta = self::DEFAULT_MAX_CALL_LIFETIME;
        }
        $this->addFieldToFilter('creation_time', array('from' => time() - $expDelta));

        return $this;
    }

    public function getInactiveAddonsCalls()
    {
        $this->addFieldToFilter('enabled', '0');

        return $this;
    }

    public function getMoreThenAddonsCalls($times = 1)
    {
        if (!is_numeric((string)$times)) {
            $times = self::DEFAULT_MAX_TIMES_CALL;
        }
        $this->addFieldToFilter('call_times', array('gteq' => $times));

        return $this;
    }

    public function cleanOldAddonsCalls($removeInactive = true, $expDelta = 0, $timesCalls = 1)
    {
        $this->clear();
        // TODO or condition
        $removeInactive ? $this->getInactiveAddonsCalls() : null;
        0 < $expDelta ? $this->getExpiredAddonsCalls($expDelta) : null;
        0 < $timesCalls ? $this->getMoreThenAddonsCalls($timesCalls) : null;

        foreach ($this->getItems() as $item) {
            $item->isDeleted(true);
            $item->save();
        }
    }
}