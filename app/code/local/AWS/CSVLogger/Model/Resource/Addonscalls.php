<?php

class AWS_CSVLogger_Model_Resource_Addonscalls extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('aws_csvlogger/addonscalls', 'id');
    }
}