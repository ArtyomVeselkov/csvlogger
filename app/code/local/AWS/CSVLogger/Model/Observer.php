<?php

// TODO sales_model_service_quote_submit_failure

class AWS_CSVLogger_Model_Observer
{
    protected static $_lastLogId = null;
    protected static $_lines = null;
    protected static $_wasActivated = false;
    /**
     * @var array strict massive of columns' names, that must be write to csv file
     */
    protected $_varsColumn = array();
    /**
     * @var array array of values, that were collected during Magento lifetime
     */
    protected $_varsValue = array();
    
    protected function _prepareColumns()
    {
        if (null == $this->_varsColumn) {
            $helper = Mage::helper('aws_csvlogger');
            $this->_varsColumn = $helper->generateWritableColumns($helper->getConfWritableColumns());
        }
        return is_array($this->_varsColumn) && count ($this->_varsColumn) > 0;
    }
    
    protected function _storeVars($event, $eventName)
    {
        if ($this->_prepareColumns() && isset($this->_varsColumn[$eventName])) {
            $helper = Mage::helper('aws_csvlogger');
            $fixer = Mage::helper('aws_csvlogger/addons');
            $register = array();
            $status = $fixer->process($eventName, $event->getData(), $register);
            try {
                $this->_varsValue = array_merge($this->_varsValue, $register);
            } catch (Exception $e) {
                ;
            }
            foreach ($this->_varsColumn[$eventName] as $data => $key) {
                foreach ($key as $fromField => $toField) {
                    try {
                        $transf = $helper->processDataField($event->getData($data), $fromField);
                        $this->_varsValue[$toField] = (null == $transf) && (isset($this->_varsValue[$toField])) ? $this->_varsValue[$toField] : $transf;
                    } catch (Exception $e) {
                        continue;
                    }
                }
            }
            if (is_array($status) && isset($status['error']) && is_string(@$status['error'])) {
                Mage::throwException($status['error']);
            }
        }
    }

    public function processEvent(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('aws_csvlogger');
        $eventName = $observer->getEvent()->getName();
        $excludedUrls = Mage::getStoreConfig('aws_csvlogger/options/exclude_url');
        if ($helper->checkForExclusion(Mage::app()->getRequest()->getPathInfo(), $helper->getExcludedUrls($excludedUrls), $eventName)) {
            return ;
        }
        $this::$_wasActivated = true;
        $this->_storeVars($observer->getEvent(), $eventName);
        return ;
    }

    public function pushToLog(Varien_Event_Observer $observer)
    {
        if (!$this::$_wasActivated) {
            return ;
        }
        $helper = Mage::helper('aws_csvlogger');
        $excludedUrls = Mage::getStoreConfig('aws_csvlogger/options/exclude_url');
        if ($helper->checkForExclusion(Mage::app()->getRequest()->getPathInfo(), $helper->getExcludedUrls($excludedUrls))) {
            return ;
        }
        $additionalVars = $helper->getAdditionalVars();
        $line = array();
        
        // TODO here should be profiles: loop models <!-- start
        
        foreach ($helper->getColumnsLayout(array_keys($this->_varsValue)) as $column => $mask) {
            $line[$column] = $helper->getFormatCell($mask, array_merge($helper->getTranslateArray(), $this->_varsValue, $additionalVars));
        }
        $helper->pushToLog($line);

        if (Mage::getStoreConfig('aws_csvlogger/options/enable_debug_log')) {
            Mage::log('After push: ' . var_export(array_merge($helper->getTranslateArray(), $this->_varsValue, $additionalVars), true), null, 'csvlogger.log');
        }
        
        // --> end
        return ;
    }
}