<?php

class AWS_CSVLogger_Model_System_Config_Source_Cms_Block
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            /** @var Mage_Cms_Model_Resource_Block_Collection $collection */
            $collection = $this->_options = Mage::getResourceModel('cms/block_collection')->load();
            $this->_options = $collection->toOptionArray();
        }
        return $this->_options;
    }

}
